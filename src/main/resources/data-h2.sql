INSERT INTO subdistribuidores (nombre) VALUES
  ('pepito');

INSERT INTO subdistribuidores (nombre) VALUES
  ('lelita');

INSERT INTO users (identification, email, password, name, isAdmin) VALUES
  (1, 'stiven140@hotmail.com', '$2a$10$HrbbMBgH7cxc2tn8PTdRiuo3baCC0PsAAJ4/IEv1Y3U0A/F6VGIXa', 'pepito', TRUE);

INSERT INTO users (identification, email, password, name, isAdmin) VALUES
  (2, 'lelita@test.com', '$2a$10$HrbbMBgH7cxc2tn8PTdRiuo3baCC0PsAAJ4/IEv1Y3U0A/F6VGIXa', 'lelita', FALSE);

INSERT INTO asesores (cedula, nombre) VALUES
  ('101510', 'mariano');

INSERT INTO employee (identification, address, recharge, leader_name, locatable, name, phone, role) VALUES
  (1010, 'calle 10 # 10 -10', 10, 'pepito', FALSE, 'pepo', 3156548985, 'WHOLESALER');

INSERT INTO employee (identification, recharge, leader_name, locatable, name, role) VALUES
  (1011, 15, 'pepito', TRUE, 'memo', 'ADVISER');

INSERT INTO employee (identification, address, recharge, leader_name, locatable, name, phone, role) VALUES
  (1012, 'calle 45 # 7 -10', 10, 'pepito', FALSE, 'brayan', 3002156598, 'WHOLESALER');

INSERT INTO employee (identification, address, recharge, leader_name, locatable, name, phone, role) VALUES
  (1013, 'calle 100 # 7 -10', 15, 'lelita', FALSE, 'maria', 3126539598, 'WHOLESALER');

INSERT INTO user (email, password, employee_identification, type) VALUES
  ('test@test.com', '$2a$10$HrbbMBgH7cxc2tn8PTdRiuo3baCC0PsAAJ4/IEv1Y3U0A/F6VGIXa', 1010, 1);

INSERT INTO simcards (numero, ICC, fecha_vencimiento, fecha_entrega, fecha_activacion, tipo, paquete, employee_identification, nombre_subdistribuidor)
VALUES
  ('3165425632', '8957123401449637755', '2018-08-05', '2017-10-12', '2017-12-05', 2, 10, 1010, 'pepito');

INSERT INTO simcards (numero, ICC, fecha_vencimiento, fecha_entrega, fecha_activacion, tipo, paquete, employee_identification, nombre_subdistribuidor)
VALUES
  ('3002271677', '8957123301419352700', '2018-09-15', '2017-12-06', NULL, 2, NULL, 1012, 'pepito');

INSERT INTO simcards (numero, ICC, fecha_vencimiento, fecha_entrega, fecha_activacion, tipo, paquete, employee_identification, nombre_subdistribuidor)
VALUES
  ('3216536598', '8957123401805176598', '2019-12-15', '2019-03-07', '2019-03-07', 1, NULL, 1013, 'pepito');

INSERT INTO simcards (numero, ICC, fecha_vencimiento, fecha_entrega, fecha_activacion, tipo, paquete, employee_identification, nombre_subdistribuidor)
VALUES
  ('3002561519', '8957123401805176612', '2016-12-15', '2016-03-07', '2016-03-07', 1, NULL, NULL, 'pepito');

INSERT INTO libres (numero, plan, valor) VALUES
  ('3165425632', '1l', 36000);

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (1, '2010-10-10', 10000, '3165425632');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (2, '2010-10-11', 20000, '3165425632');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (3, '2010-11-11', 15000, '3165425632');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (4, '2010-11-11', 18000, '3002271677');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (5, '2010-11-11', 32000, '3002271677');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (6, '2011-11-13', 40000, '3002561519');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (7, '2011-12-13', 4000, '3002561519');

INSERT INTO recargas (id, fecha_recarga, valor_recarga, telefono) VALUES
  (8, '2013-01-13', 10000, '3002561519');