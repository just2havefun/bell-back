package org.belltronic.wholesalers.crosscutting.util;

import java.math.BigDecimal;

public final class AccountingConstants {

   public static final BigDecimal IVA_PERCENTAGE = BigDecimal.valueOf(0.19);
   public static final BigDecimal ICA_PERCENTAGE = BigDecimal.valueOf(9.6 / 1000);
   public static final BigDecimal RETE_PERCENTAGE = BigDecimal.valueOf(0.11);
}
