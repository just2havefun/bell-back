package org.belltronic.wholesalers.crosscutting.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class FileLoader {

   public static InputStream readFileFromResources(final String fileName) {
      ClassLoader cl = FileLoader.class.getClassLoader();
      return cl.getResourceAsStream(fileName);
   }

   public static File convertMultiPartToFile(MultipartFile multipartFile) throws IOException {
      String fileName = Objects.requireNonNull(multipartFile.getOriginalFilename());
      File file = File.createTempFile(fileName, ".csv");
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(multipartFile.getBytes());
      fos.close();
      return file;
   }
}
