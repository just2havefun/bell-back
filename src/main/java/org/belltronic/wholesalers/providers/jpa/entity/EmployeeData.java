package org.belltronic.wholesalers.providers.jpa.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.belltronic.wholesalers.domain.entity.Employee;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employee")
@NoArgsConstructor
@Getter
@Setter
public class EmployeeData {

   @Id
   @Column(name = "identification")
   private Long identification;

   @NotNull
   @Column(name = "name")
   private String name;

   @Column(name = "locatable")
   private Boolean locatable = false;

   @Column(name = "phone")
   private Long phone;

   @Column(name = "address")
   private String address;

   @NotNull
   @Column(name = "commission")
   private Integer commission;

   @NotNull
   @Column(name = "leader_name")
   private String leaderName;

   @NotNull
   @Column(name = "role")
   private Employee.Role role;

}
