package org.belltronic.wholesalers.providers.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comisiones")
public class CommissionData {

   @Id
   private Long id;

   @Column(name = "telefono")
   private String number;

   @Column(name = "valor")
   private int value;

   @Column(name = "periodo")
   private String period;

   public CommissionData() {
      /* Empty constructor required by JPA */
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNumber() {
      return number;
   }

   public void setNumber(String number) {
      this.number = number;
   }

   public int getValue() {
      return value;
   }

   public void setValue(int value) {
      this.value = value;
   }

   public String getPeriod() {
      return period;
   }

   public void setPeriod(String period) {
      this.period = period;
   }
}
