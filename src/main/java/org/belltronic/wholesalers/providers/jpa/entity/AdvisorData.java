package org.belltronic.wholesalers.providers.jpa.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "asesores")
@NoArgsConstructor
@Getter
@Setter
public class AdvisorData {

   @Id
   @Column(name = "cedula")
   private Long identification;

   @NotNull
   @Column(name = "nombre")
   private String name;

}
