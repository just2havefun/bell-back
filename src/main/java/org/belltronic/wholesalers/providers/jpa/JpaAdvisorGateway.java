package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.Advisor;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.AdvisorGateway;
import org.belltronic.wholesalers.providers.jpa.entity.AdvisorData;
import org.belltronic.wholesalers.providers.jpa.repository.AdvisorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaAdvisorGateway implements AdvisorGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   @Autowired
   private AdvisorRepository advisorRepository;

   @Override
   public Advisor readAdvisor(Long identification) {
      AdvisorData advisorData = advisorRepository.findById(identification).orElseThrow(NotFoundException::new);
      return modelMapper.map(advisorData, Advisor.class);
   }
}
