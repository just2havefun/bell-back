package org.belltronic.wholesalers.providers.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "recargas")
public class RechargeData {

   @Id
   private Long id;

   @Column(name = "telefono")
   private String number;

   @Column(name = "fecha_recarga")
   private Date date;

   @Column(name = "valor_recarga")
   private int value;

   public RechargeData() {
      /* Empty constructor required by JPA */
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getNumber() {
      return number;
   }

   public void setNumber(String number) {
      this.number = number;
   }

   public Date getDate() {
      return date;
   }

   public void setDate(Date date) {
      this.date = date;
   }

   public int getValue() {
      return value;
   }

   public void setValue(int value) {
      this.value = value;
   }
}
