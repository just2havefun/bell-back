package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.entity.Projection;
import org.belltronic.wholesalers.domain.entity.report.CommissionReport;
import org.belltronic.wholesalers.domain.entity.report.RechargeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.DetailCommission;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.belltronic.wholesalers.providers.jpa.entity.EmployeeData;
import org.belltronic.wholesalers.providers.jpa.repository.EmployeeRepository;
import org.belltronic.wholesalers.providers.jpa.repository.RechargeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.belltronic.wholesalers.crosscutting.util.AccountingConstants.ICA_PERCENTAGE;
import static org.belltronic.wholesalers.crosscutting.util.AccountingConstants.IVA_PERCENTAGE;
import static org.belltronic.wholesalers.crosscutting.util.AccountingConstants.RETE_PERCENTAGE;

@Service
public class JpaRechargeGateway implements RechargeGateway {

   private RechargeRepository rechargeRepository;
   private EmployeeRepository employeeRepository;
   private SimpleDateFormat parser = new SimpleDateFormat("yyyy/MM/dd");

   @PersistenceContext
   private EntityManager manager;

   @Autowired
   public JpaRechargeGateway(RechargeRepository rechargeRepository, EmployeeRepository employeeRepository) {
      this.rechargeRepository = rechargeRepository;
      this.employeeRepository = employeeRepository;
   }

   @Override
   public Set<String> getAvailablePeriods(Long employeeIdentification) {
      return rechargeRepository.getAvailablePeriods(employeeIdentification).stream().map(String::valueOf)
            .collect(Collectors.toSet());
   }

   @Override
   public CommissionReport getCommissionReport(Long employeeIdentification, Integer year, Integer month) throws ParseException {
      EmployeeData employee = employeeRepository.findById(employeeIdentification).orElseThrow(NotFoundException::new);
      Date dueDate = parser.parse(year + "/" + month + "/1");
      List<Object[]> data = rechargeRepository.getCommissionReport(employeeIdentification, year, month, dueDate);
      CommissionReport commissionReportDTO = new CommissionReport(employeeIdentification, (String) data.get(0)[0], year, month);
      commissionReportDTO.setPrepaidCommission((BigDecimal) data.get(0)[1]);
      commissionReportDTO.setFreeCommission((BigDecimal) data.get(0)[2]);
      updateFees(commissionReportDTO, employee.getCommission());
      return commissionReportDTO;
   }

   @Override
   public List<CommissionReport> getCommissionsReport(Long employeeIdentification) {
      EmployeeData employee = employeeRepository.findById(employeeIdentification).orElseThrow(NotFoundException::new);
      Date dueDate = new Date();
      List<Object[]> data = rechargeRepository.getCommissionsReport(employeeIdentification, dueDate);
      return data.stream().map(row -> {
         CommissionReport commissionReportDTO = new CommissionReport(employeeIdentification, (String) row[0], (int) row[3],
               (int) row[4]);
         commissionReportDTO.setPrepaidCommission((BigDecimal) row[1]);
         commissionReportDTO.setFreeCommission((BigDecimal) row[2]);
         updateFees(commissionReportDTO, employee.getCommission());
         return commissionReportDTO;
      }).collect(Collectors.toList());
   }

   @Override
   public List<CommissionReport> getSubCommissionsReport(String subDistributorName, Integer year, Integer month) throws ParseException {
      Date dueDate = parser.parse(year + "/" + month + "/1");
      List<Object[]> data = rechargeRepository.getSubDistributorCommissionsReport(subDistributorName, year, month, dueDate);
      return data.stream().map(row -> {
         BigInteger identification = (BigInteger) row[0];
         EmployeeData employee = employeeRepository.findById(identification.longValue()).orElseThrow(NotFoundException::new);
         String name = (String) row[1];
         CommissionReport commissionReportDTO = new CommissionReport(identification.longValue(), name, year,
               month);
         commissionReportDTO.setPrepaidCommission((BigDecimal) row[2]);
         commissionReportDTO.setFreeCommission((BigDecimal) row[3]);
         updateFees(commissionReportDTO, employee.getCommission());
         return commissionReportDTO;
      }).collect(Collectors.toList());

   }

   @Override
   public List<DetailCommission> getCommissionDetail(Long employeeIdentification, Integer year, Integer month) throws ParseException {
      EmployeeData employee = employeeRepository.findById(employeeIdentification).orElseThrow(NotFoundException::new);
      Date dueDate = parser.parse(year + "/" + month + "/1");
      List<Object[]> data = rechargeRepository.getDetailCommissionReport(employeeIdentification, year, month, dueDate);
      List<DetailCommission> details = new ArrayList<>();
      for (Object[] detail : data) {
         details.add(new DetailCommission(
               (String) detail[0],
               (String) detail[1],
               (String) detail[2],
               (int) detail[3],
               (Date) detail[4],
               (Date) detail[5],
               (Date) detail[6],
               BigInteger.valueOf((int) detail[7]),
               (String) detail[8]));
      }
      details.forEach((detail) -> updateFees(detail, employee.getCommission()));
      return details;
   }

   @Override
   public Projection getProjection(Long identification, Integer commission, Integer year, Integer month) throws ParseException {
      Optional<Number> lastDayPrepaidOpt = rechargeRepository.getLastRechargeDayByType(identification, year, month, 1);
      Optional<Number> lastDayFreeOpt = rechargeRepository.getLastRechargeDayByType(identification, year, month, 2);
      Date dueDate = parser.parse(year + "/" + month + "/1");
      List<Object[]> data = rechargeRepository.getCommissionReport(identification, year, month, dueDate);
      Projection projectionDTO = new Projection(identification, (String) data.get(0)[0], year, month);
      BigDecimal prepaidCommission = (BigDecimal) data.get(0)[1];

      BigDecimal monthDays = BigDecimal.valueOf(30);

      lastDayPrepaidOpt.ifPresent((lastDayPrepaid -> {
         projectionDTO.setPrepaidCommission(prepaidCommission.divide(BigDecimal.valueOf(lastDayPrepaid.longValue()), RoundingMode.FLOOR).multiply(monthDays));
      }));

      BigDecimal freeCommission = (BigDecimal) data.get(0)[2];

      lastDayFreeOpt.ifPresent((lastDayFree -> {
         projectionDTO.setFreeCommission(freeCommission.divide(BigDecimal.valueOf(lastDayFree.longValue()), RoundingMode.FLOOR).multiply(monthDays));
      }));

      updateFees(projectionDTO, commission);

      return projectionDTO;
   }

   @Override
   @Transactional
   public void cleanRecharges() {
      manager.createNativeQuery("DELETE FROM recargas").executeUpdate();
   }

   @Override
   public List<RechargeReport> getSubRechargeReport(String subDistributorName, Integer year, Integer month) throws ParseException {
      List<EmployeeData> employees = employeeRepository.listEmployeesByRoleAndLeaderName(Employee.Role.WHOLESALER, subDistributorName);
      Date dueDate = parser.parse(year + "/" + month + "/1");
      return employees.stream().map(employeeData -> {
         Object[] row = rechargeRepository.getCommissionReport(employeeData.getIdentification(), year, month, dueDate).get(0);
         RechargeReport rechargeReport = RechargeReport.builder()
               .employeeIdentification(employeeData.getIdentification())
               .freeRecharge((BigDecimal) row[2])
               .prepaidRecharge((BigDecimal) row[1])
               .year(year)
               .month(month)
               .employeeName(employeeData.getName())
               .build();
         return rechargeReport;
      }).collect(Collectors.toList());
   }

   private void updateFees(Projection projection, Integer commissionPercentage) {
      BigDecimal commissionDecimal = BigDecimal.valueOf(commissionPercentage).setScale(3, RoundingMode.UNNECESSARY).divide(BigDecimal.valueOf(100), RoundingMode.UNNECESSARY);
      BigDecimal commission = BigDecimal.valueOf(projection.getPrepaidCommission().longValue());
      BigDecimal iva = commission.multiply(IVA_PERCENTAGE);
      commission = commission.subtract(iva);
      commission = commission.multiply(commissionDecimal);
      BigDecimal ica = commission.multiply(ICA_PERCENTAGE);
      BigDecimal rete = commission.multiply(RETE_PERCENTAGE);
      projection.setPrepaidCommission(commission.subtract(ica).subtract(rete));

      commission = BigDecimal.valueOf(projection.getFreeCommission().longValue());
      iva = commission.multiply(IVA_PERCENTAGE);
      commission = commission.subtract(iva);
      commission = commission.multiply(commissionDecimal);
      ica = commission.multiply(ICA_PERCENTAGE);
      rete = commission.multiply(RETE_PERCENTAGE);
      projection.setFreeCommission(commission.subtract(ica).subtract(rete));
   }

   private void updateFees(CommissionReport commissionReport, Integer commission) {
      BigDecimal commissionDecimal = BigDecimal.valueOf(commission).setScale(3, RoundingMode.UNNECESSARY).divide(BigDecimal.valueOf(100), RoundingMode.UNNECESSARY);
      BigDecimal prepaidCommission = commissionReport.getPrepaidCommission();
      BigDecimal prepaidIva = prepaidCommission.multiply(IVA_PERCENTAGE);
      prepaidCommission = prepaidCommission.subtract(prepaidIva);
      prepaidCommission = prepaidCommission.multiply(commissionDecimal);
      commissionReport.setPrepaidCommission(prepaidCommission);
      BigDecimal prepaidIca = prepaidCommission.multiply(ICA_PERCENTAGE);
      BigDecimal prepaidRete = prepaidCommission.multiply(RETE_PERCENTAGE);
      commissionReport.setPrepaidIca(prepaidIca);
      commissionReport.setPrepaidRete(prepaidRete);

      BigDecimal freeCommission = commissionReport.getFreeCommission();
      BigDecimal freeIva = freeCommission.multiply(IVA_PERCENTAGE);
      freeCommission = freeCommission.subtract(freeIva);
      freeCommission = freeCommission.multiply(commissionDecimal);
      commissionReport.setFreeCommission(freeCommission);
      BigDecimal freeIca = freeCommission.multiply(ICA_PERCENTAGE);
      BigDecimal freeRete = freeCommission.multiply(RETE_PERCENTAGE);
      commissionReport.setFreeIca(freeIca);
      commissionReport.setFreeRete(freeRete);
   }

   private void updateFees(DetailCommission detailCommission, Integer commissionPercentage) {
      BigDecimal commissionDecimal = BigDecimal.valueOf(commissionPercentage).setScale(3, RoundingMode.UNNECESSARY).divide(BigDecimal.valueOf(100), RoundingMode.UNNECESSARY);
      BigDecimal commission = BigDecimal.valueOf(detailCommission.getRecharge().longValue());
      BigDecimal iva = commission.multiply(IVA_PERCENTAGE);
      detailCommission.setNeto(commission.subtract(iva).toBigInteger());
      commission = commission.subtract(iva);
      commission = commission.multiply(commissionDecimal);
      BigDecimal ica = commission.multiply(ICA_PERCENTAGE);
      BigDecimal rete = commission.multiply(RETE_PERCENTAGE);
      detailCommission.setComission(commission.subtract(ica).subtract(rete).toBigInteger());
   }

   private void updateFees(RechargeReport projection) {
      BigDecimal prepaid = projection.getPrepaidRecharge();
      BigDecimal ica = prepaid.multiply(ICA_PERCENTAGE);
      BigDecimal rete = prepaid.multiply(RETE_PERCENTAGE);
      projection.setPrepaidRecharge(prepaid.subtract(ica).subtract(rete));

      BigDecimal free = projection.getFreeRecharge();
      ica = free.multiply(ICA_PERCENTAGE);
      rete = free.multiply(RETE_PERCENTAGE);
      projection.setFreeRecharge(free.subtract(ica).subtract(rete));
   }
}
