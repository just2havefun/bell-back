package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.report.ProductionReport;
import org.belltronic.wholesalers.domain.gateway.CommissionGateway;
import org.belltronic.wholesalers.providers.jpa.repository.CommissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class JpaCommissionGateway implements CommissionGateway {

   private CommissionRepository commissionRepository;

   @Autowired
   public JpaCommissionGateway(CommissionRepository commissionRepository) {
      this.commissionRepository = commissionRepository;
   }

   @Override
   public Set<String> getAvailablePeriods(Long employeeIdentification) {
      return commissionRepository.getAvailablePeriods(employeeIdentification)
            .stream()
            .map(String::valueOf)
            .collect(Collectors.toSet());
   }

   @Override
   public Set<String> getSubAvailablePeriods(String name) {
      return commissionRepository.getSubAvailablePeriods(name)
            .stream()
            .map(String::valueOf)
            .collect(Collectors.toSet());
   }

   @Override
   public List<ProductionReport> getSubProductionReport(String subDistributorName, String period, Date dueDate) {
      List<Object[]> data = commissionRepository.getSubDistributorCommissionsReport(subDistributorName, period, dueDate);
      return data.stream().map(row -> {
         BigInteger identification = (BigInteger) row[0];
         return new ProductionReport(identification.longValue(),
               (String) row[1],
               (String) row[2],
               (BigDecimal) row[3],
               (BigDecimal) row[4]);
      }).collect(Collectors.toList());
   }

}
