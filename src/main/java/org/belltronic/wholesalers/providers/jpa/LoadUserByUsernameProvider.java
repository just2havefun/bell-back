package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.providers.jpa.entity.ApplicationUserData;
import org.belltronic.wholesalers.providers.jpa.entity.SubDistributorData;
import org.belltronic.wholesalers.providers.jpa.repository.OldUserRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SubDistributorRepository;
import org.belltronic.wholesalers.providers.jpa.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoadUserByUsernameProvider implements UserDetailsService {

   private static final ModelMapper modelMapper = new ModelMapper();

   private UserRepository userRepository;
   private OldUserRepository oldUserRepository;
   private SubDistributorRepository subDistributorRepository;

   @Autowired
   public LoadUserByUsernameProvider(UserRepository userRepository, OldUserRepository oldUserRepository, SubDistributorRepository subDistributorRepository) {
      this.userRepository = userRepository;
      this.oldUserRepository = oldUserRepository;
      this.subDistributorRepository = subDistributorRepository;
   }

   @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      ApplicationUser user;
      try {
         user = oldUserRepository.getApplicationUserByEmail(username).orElseThrow(NotFoundException::new);
      } catch (NotFoundException ie) {
         try {
            SubDistributorData subDistributorData = subDistributorRepository.readSubDistributorByEmail(username).orElseThrow(NotFoundException::new);
            user = new ApplicationUser(subDistributorData.getEmail(), subDistributorData.getPassword(), subDistributorData.getName(), false);
         } catch (NotFoundException e) {
            try {
               ApplicationUserData applicationUserData = userRepository.findById(username).orElseThrow(NotFoundException::new);
               user = modelMapper.map(applicationUserData, ApplicationUser.class);
            } catch (NotFoundException ne) {
               throw new UsernameNotFoundException(ne.getMessage());
            }
         }
      }
      return new User(user.getEmail(), user.getPassword(), AuthorityUtils.createAuthorityList());
   }
}
