package org.belltronic.wholesalers.providers.jpa;

import org.apache.commons.lang.RandomStringUtils;
import org.belltronic.wholesalers.domain.entity.SubDistributor;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.SubdistributorGateway;
import org.belltronic.wholesalers.providers.jpa.entity.SubDistributorData;
import org.belltronic.wholesalers.providers.jpa.repository.SubDistributorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JpaSubdistributorGateway implements SubdistributorGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   private SubDistributorRepository subDistributorRepository;

   @Autowired
   public JpaSubdistributorGateway(SubDistributorRepository subDistributorRepository) {
      this.subDistributorRepository = subDistributorRepository;
   }

   @Override
   public SubDistributor readSubdistributor(String name) {
      SubDistributorData subDistributorData = subDistributorRepository.findById(name).orElseThrow(NotFoundException::new);
      return modelMapper.map(subDistributorData, SubDistributor.class);
   }

   @Override
   public SubDistributor readSubdistributorByEmail(String email) {
      SubDistributorData subDistributorData = subDistributorRepository.readSubDistributorByEmail(email)
            .orElseThrow(NotFoundException::new);
      return modelMapper.map(subDistributorData, SubDistributor.class);
   }

   @Override
   public List<SubDistributor> listSubDistributors() {
      return subDistributorRepository.findAll().stream()
            .map(subDistributorData -> modelMapper.map(subDistributorData, SubDistributor.class))
            .collect(Collectors.toList());
   }

   @Override
   public List<SubDistributor> listSubDistributorsByLeader(String leaderEmail) {
      return subDistributorRepository.listSubDistributorsByLeaderEmail(leaderEmail).stream()
            .map(subDistributorData -> modelMapper.map(subDistributorData, SubDistributor.class))
            .collect(Collectors.toList());
   }

   @Override
   public SubDistributor updateSubdistributor(SubDistributor subDistributor) {
      SubDistributorData subDistributorData = subDistributorRepository.findById(subDistributor.getName())
            .orElseThrow(NotFoundException::new);
      subDistributorData.setCommission(subDistributor.getCommission());
      String password = RandomStringUtils.random(10, true, true);
      if (!subDistributor.getEmail().equals(subDistributorData.getEmail())) {
         subDistributorData.setEmail(subDistributor.getEmail());
         subDistributorData.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
      }
      subDistributorData = subDistributorRepository.save(subDistributorData);
      subDistributorData.setPassword(password);
      return modelMapper.map(subDistributorData, SubDistributor.class);
   }
}
