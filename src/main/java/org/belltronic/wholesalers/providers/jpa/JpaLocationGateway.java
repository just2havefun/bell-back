package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.Location;
import org.belltronic.wholesalers.domain.gateway.LocationGateway;
import org.belltronic.wholesalers.providers.jpa.entity.LocationData;
import org.belltronic.wholesalers.providers.jpa.repository.LocationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JpaLocationGateway implements LocationGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   private LocationRepository locationRepository;

   @Autowired
   public JpaLocationGateway(LocationRepository locationRepository) {
      this.locationRepository = locationRepository;
   }

   @Override
   public Location createLocation(Location location) {
      LocationData locationData = modelMapper.map(location, LocationData.class);
      LocationData storedLocation = locationRepository.save(locationData);
      return modelMapper.map(storedLocation, Location.class);
   }
}
