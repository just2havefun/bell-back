package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.RechargeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RechargeRepository extends JpaRepository<RechargeData, Long> {

   @Query(value = "SELECT e.name, COALESCE(SUM(CASE WHEN s.tipo = 1 THEN r.valor_recarga END),0) prepaidCommission, "
         + "COALESCE(SUM(CASE WHEN s.tipo = 2 THEN r.valor_recarga END),0) freeCommission FROM recargas r "
         + "INNER JOIN simcards s ON r.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.identification = :identification AND YEAR(r.fecha_recarga) = :year AND MONTH(r.fecha_recarga) = :month "
         + "AND DATE_ADD(s.fecha_activacion, INTERVAL 8 MONTH) > :dueDate", nativeQuery = true)
   List<Object[]> getCommissionReport(@Param("identification") Long identification, @Param("year") int year,
                                      @Param("month") int month, @Param("dueDate") Date dueDate);

   @Query(value = "SELECT e.name, COALESCE(SUM(CASE WHEN s.tipo = 1 THEN r.valor_recarga END),0) prepaidCommission, "
         + "COALESCE(SUM(CASE WHEN s.tipo = 2 THEN r.valor_recarga END),0) freeCommission, YEAR(r.fecha_recarga), MONTH(r.fecha_recarga) FROM recargas r "
         + "INNER JOIN simcards s ON r.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.identification = :identification "
         + "AND DATE_ADD(s.fecha_activacion, INTERVAL 8 MONTH) > :dueDate "
         + "GROUP BY YEAR(r.fecha_recarga), MONTH(r.fecha_recarga)", nativeQuery = true)
   List<Object[]> getCommissionsReport(@Param("identification") Long identification, @Param("dueDate") Date dueDate);

   @Query(value = "SELECT e.identification, e.name, COALESCE(SUM(CASE WHEN s.tipo = 1 THEN r.valor_recarga END),0) prepaidCommission, " +
         "COALESCE(SUM(CASE WHEN s.tipo = 2 THEN r.valor_recarga END),0) freeCommission FROM recargas r " +
         "INNER JOIN simcards s ON r.telefono = s.numero " +
         "INNER JOIN employee e ON s.employee_identification = e.identification " +
         "WHERE e.leader_name = :name " +
         "AND YEAR(r.fecha_recarga) = :year " +
         "AND MONTH(r.fecha_recarga) =  :month " +
         "AND DATE_ADD(s.fecha_activacion, INTERVAL 8 MONTH) > :dueDate " +
         "GROUP BY e.identification", nativeQuery = true)
   List<Object[]> getSubDistributorCommissionsReport(@Param("name") String name, @Param("year") int year,
                                                     @Param("month") int month, @Param("dueDate") Date dueDate);

   @Query(value = "SELECT DISTINCT (CONCAT(YEAR(r.fecha_recarga),'-', MONTH(r.fecha_recarga))) FROM recargas r "
         + "INNER JOIN simcards s ON r.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.identification = :identification", nativeQuery = true)
   Set<Object> getAvailablePeriods(@Param("identification") Long identification);

   @Query(value = "SELECT DAY(r.fecha_recarga)" +
         "FROM recargas r " +
         "INNER JOIN simcards s ON r.telefono = s.numero " +
         "INNER JOIN employee e ON s.employee_identification = e.identification " +
         "WHERE e.identification = :identification AND YEAR(r.fecha_recarga) = :year AND MONTH(r.fecha_recarga) = :month AND s.tipo= :type " +
         "ORDER BY r.fecha_recarga DESC " +
         "LIMIT 1", nativeQuery = true)
   Optional<Number> getLastRechargeDayByType(@Param("identification") Long identification, @Param("year") int year,
                                             @Param("month") int month, @Param("type") int type);

   @Query(value = "SELECT e.name, s.numero, s.icc, s.tipo, s.fecha_entrega, s.fecha_activacion,"
         + " s.fecha_vencimiento, r.valor_recarga, "
         + "CASE "
         + " WHEN s.fecha_vencimiento > current_date and s.fecha_activacion is null THEN 'Disponible' "
         + " WHEN s.fecha_vencimiento > current_date and s.fecha_activacion is not null THEN 'Activada' "
         + " WHEN s.fecha_vencimiento < current_date THEN 'Vencida' "
         + " ELSE 'Sin Informacion' END "
         + "FROM recargas r "
         + "INNER JOIN simcards s ON r.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.identification = :identification "
         + "AND YEAR(r.fecha_recarga) = :year "
         + "AND MONTH(r.fecha_recarga) = :month "
         + "AND DATE_ADD(s.fecha_activacion, INTERVAL 8 MONTH) > :dueDate", nativeQuery = true)
   List<Object[]> getDetailCommissionReport(@Param("identification") Long identification, @Param("year") int year,
                                            @Param("month") int month, @Param("dueDate") Date dueDate);
}

