package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.providers.jpa.entity.EmployeeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeData, Long> {

   @Query("SELECT e FROM EmployeeData e WHERE e.leaderName = :leader_name and e.role = :role")
   List<EmployeeData> listEmployeesByRoleAndLeaderName(@Param("role") Employee.Role role,
                                                       @Param("leader_name") String leaderName);

   @Query("SELECT e FROM EmployeeData e WHERE e.role = :role")
   List<EmployeeData> listEmployeeByRole(@Param("role") Employee.Role role);

}

