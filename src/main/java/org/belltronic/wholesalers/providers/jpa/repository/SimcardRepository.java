package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.domain.entity.report.csv.EmployeeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport;
import org.belltronic.wholesalers.providers.jpa.entity.SimcardData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SimcardRepository extends JpaRepository<SimcardData, String> {

   Optional<SimcardData> getSimcardByIcc(String Icc);

   @Query("SELECT s FROM SimcardData s WHERE s.type= :type and s.subDistributorData.name = :leader_name")
   List<SimcardData> listSimcardsByTypeAndLeaderName(@Param("type") Integer type,
                                                     @Param("leader_name") String leaderName);

   @Query("SELECT new org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport" +
         "(sd.name," +
         " s.subDistributorData.name," +
         " e.name, " +
         " s.pack, " +
         " s.packOrder, " +
         " s.number," +
         " s.icc," +
         " s.deliveryDate, " +
         " s.activationDate, " +
         " s.dueDate," +
         " CASE " +
         " WHEN s.dueDate > current_date and s.activationDate is null THEN 'Disponible' " +
         " WHEN s.dueDate > current_date and s.activationDate is not null THEN 'Activada' " +
         " WHEN s.dueDate < current_date THEN 'Vencida' " +
         " ELSE 'Sin Informacion' END," +
         " f.enterpriseName," +
         " f.enterpriseNit," +
         " f.enterpriseAddress," +
         " f.codScl," +
         " f.codPunto," +
         " f.plan," +
         " f.value," +
         " f.responsible," +
         " f.responsibleIdentification," +
         " f.responsibleAddress," +
         " f.responsibleCity," +
         " f.responsibleNeighborhood," +
         " f.responsiblePhone," +
         " f.callDetail," +
         " f.callDate) " +
         "FROM SimcardData s " +
         "INNER JOIN s.subDistributorData sd " +
         "LEFT JOIN s.freeData f " +
         "LEFT JOIN s.employeeData e " +
         "WHERE s.type= :type and sd.name = :leader_name " +
         "AND s.deliveryDate >= :initial_date " +
         "AND s.deliveryDate <= :final_date " +
         "AND s.dueDate > current_date " +
         "ORDER BY sd.name, s.subDistributorData.name, s.pack, s.packOrder")
   List<SimcardReport> listSimcardsReportByTypeAndLeaderName(@Param("type") Integer type,
                                                             @Param("leader_name") String leaderName,
                                                             @Param("initial_date") Date initialDate,
                                                             @Param("final_date") Date finalDate);

   @Query("SELECT new org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport" +
         "(sd.name," +
         " s.subDistributorData.name," +
         " e.name, " +
         " s.pack, " +
         " s.packOrder, " +
         " s.number," +
         " s.icc," +
         " s.deliveryDate, " +
         " s.activationDate, " +
         " s.dueDate," +
         " CASE " +
         " WHEN s.dueDate > current_date and s.activationDate is null THEN 'Disponible' " +
         " WHEN s.dueDate > current_date and s.activationDate is not null THEN 'Activada' " +
         " WHEN s.dueDate < current_date THEN 'Vencida' " +
         " ELSE 'Sin Informacion' END," +
         " f.enterpriseName," +
         " f.enterpriseNit," +
         " f.enterpriseAddress," +
         " f.codScl," +
         " f.codPunto," +
         " f.plan," +
         " f.value," +
         " f.responsible," +
         " f.responsibleIdentification," +
         " f.responsibleAddress," +
         " f.responsibleCity," +
         " f.responsibleNeighborhood," +
         " f.responsiblePhone," +
         " f.callDetail," +
         " f.callDate) " +
         "FROM SimcardData s " +
         "INNER JOIN s.subDistributorData sd " +
         "LEFT JOIN s.freeData f " +
         "LEFT JOIN s.employeeData e " +
         "WHERE s.type= :type " +
         "AND s.deliveryDate >= :initial_date " +
         "AND s.deliveryDate <= :final_date " +
         "AND s.dueDate > current_date " +
         "ORDER BY sd.name, s.subDistributorData.name, s.pack, s.packOrder")
   List<SimcardReport> listSimcardsReportByType(@Param("type") Integer type,
                                                @Param("initial_date") Date initialDate,
                                                @Param("final_date") Date finalDate);

   @Query("SELECT new org.belltronic.wholesalers.domain.entity.report.csv.EmployeeReport" +
         "(e.name, " +
         " CASE " +
         " WHEN s.type = 1 THEN 'Prepago' " +
         " WHEN s.type = 2 THEN 'Libre' " +
         " ELSE 'No definido' END," +
         " s.pack, " +
         " s.packOrder, " +
         " s.number," +
         " s.icc," +
         " s.deliveryDate," +
         " s.activationDate," +
         " s.dueDate," +
         " CASE " +
         " WHEN s.dueDate > current_date and s.activationDate is null THEN 'Disponible' " +
         " WHEN s.dueDate > current_date and s.activationDate is not null THEN 'Activada' " +
         " WHEN s.dueDate < current_date THEN 'Vencida' " +
         " ELSE 'Sin Informacion' END," +
         " f.codScl," +
         " f.codPunto," +
         " f.plan," +
         " f.value) " +
         "FROM SimcardData s " +
         "LEFT JOIN s.freeData f " +
         "INNER JOIN s.employeeData e " +
         "WHERE e.identification = :identification " +
         "AND s.dueDate > current_date " +
         "ORDER BY s.type, s.pack, s.packOrder")
   List<EmployeeReport> listSimcardsReportByEmployee(@Param("identification") Long identification);

   Optional<SimcardData> findFirstByOrderByPackDesc();

   List<SimcardData> findAllByPack(int packNumber);

   List<SimcardData> findAllByPackOrderByPackOrder(int packNumber);

   @Modifying
   @Query("DELETE FROM SimcardData s where s.number = :number and s.icc <> :icc ")
   void deleteConflictedNumberByIcc(@Param("number") String number, @Param("icc") String icc);

}

