package org.belltronic.wholesalers.providers.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "libres")
public class FreeData {

   @Id
   @Column(name = "numero")
   private String number;

   @NotNull
   @Column(name = "plan")
   private String plan;

   @Column(name = "valor")
   private int value;

   @Column(name = "nombre_empresa")
   private String enterpriseName;

   @Column(name = "NIT")
   private String enterpriseNit;

   @Column(name = "direccion_empresa")
   private String enterpriseAddress;

   @Column(name = "cod_scl")
   private String codScl;

   @Column(name = "cod_punto")
   private String codPunto;

   @Column(name = "responsable")
   private String responsible;

   @Column(name = "cedula")
   private String responsibleIdentification;

   @Column(name = "direccion_responsable")
   private String responsibleAddress;

   @Column(name = "ciudad_responsable")
   private String responsibleCity;

   @Column(name = "barrio_responsable")
   private String responsibleNeighborhood;

   @Column(name = "telefono")
   private String responsiblePhone;

   @Column(name = "detalle_llamada")
   private String callDetail;

   @Column(name = "fecha_llamada")
   private Date callDate;

   public FreeData() {
      /* Empty constructor required by JPA */
   }

   public String getNumber() {
      return number;
   }

   public void setNumber(String number) {
      this.number = number;
   }

   public String getPlan() {
      return plan;
   }

   public void setPlan(String plan) {
      this.plan = plan;
   }

   public int getValue() {
      return value;
   }

   public void setValue(int value) {
      this.value = value;
   }

   public String getEnterpriseName() {
      return enterpriseName;
   }

   public void setEnterpriseName(String enterpriseName) {
      this.enterpriseName = enterpriseName;
   }

   public String getEnterpriseNit() {
      return enterpriseNit;
   }

   public void setEnterpriseNit(String enterpriseNit) {
      this.enterpriseNit = enterpriseNit;
   }

   public String getEnterpriseAddress() {
      return enterpriseAddress;
   }

   public void setEnterpriseAddress(String enterpriseAddress) {
      this.enterpriseAddress = enterpriseAddress;
   }

   public String getCodScl() {
      return codScl;
   }

   public void setCodScl(String codScl) {
      this.codScl = codScl;
   }

   public String getCodPunto() {
      return codPunto;
   }

   public void setCodPunto(String codPunto) {
      this.codPunto = codPunto;
   }

   public String getResponsible() {
      return responsible;
   }

   public void setResponsible(String responsible) {
      this.responsible = responsible;
   }

   public String getResponsibleIdentification() {
      return responsibleIdentification;
   }

   public void setResponsibleIdentification(String responsibleIdentification) {
      this.responsibleIdentification = responsibleIdentification;
   }

   public String getResponsibleAddress() {
      return responsibleAddress;
   }

   public void setResponsibleAddress(String responsibleAddress) {
      this.responsibleAddress = responsibleAddress;
   }

   public String getResponsibleCity() {
      return responsibleCity;
   }

   public void setResponsibleCity(String responsibleCity) {
      this.responsibleCity = responsibleCity;
   }

   public String getResponsibleNeighborhood() {
      return responsibleNeighborhood;
   }

   public void setResponsibleNeighborhood(String responsibleNeighborhood) {
      this.responsibleNeighborhood = responsibleNeighborhood;
   }

   public String getResponsiblePhone() {
      return responsiblePhone;
   }

   public void setResponsiblePhone(String responsiblePhone) {
      this.responsiblePhone = responsiblePhone;
   }

   public String getCallDetail() {
      return callDetail;
   }

   public void setCallDetail(String callDetail) {
      this.callDetail = callDetail;
   }

   public Date getCallDate() {
      return callDate;
   }

   public void setCallDate(Date callDate) {
      this.callDate = callDate;
   }
}
