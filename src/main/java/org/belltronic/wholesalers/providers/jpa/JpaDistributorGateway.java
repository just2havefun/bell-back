package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.Distributor;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.DistributorGateway;
import org.belltronic.wholesalers.providers.jpa.entity.OldUserData;
import org.belltronic.wholesalers.providers.jpa.repository.OldUserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class JpaDistributorGateway implements DistributorGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   private OldUserRepository oldUserRepository;

   @Autowired
   public JpaDistributorGateway(OldUserRepository oldUserRepository) {
      this.oldUserRepository = oldUserRepository;
   }

   @Override
   public Distributor readDistributorByEmail(String email) {
      OldUserData oldUserData = oldUserRepository.getOldUserDataByEmail(email).orElseThrow(NotFoundException::new);
      return modelMapper.map(oldUserData, Distributor.class);
   }

   @Override
   public List<Distributor> listDistributors() {
      return oldUserRepository.findAll().stream()
            .map(oldUserData -> modelMapper.map(oldUserData, Distributor.class))
            .collect(Collectors.toList());
   }
}
