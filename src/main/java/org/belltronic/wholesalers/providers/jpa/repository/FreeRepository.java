package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.FreeData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FreeRepository extends JpaRepository<FreeData, String> {

}

