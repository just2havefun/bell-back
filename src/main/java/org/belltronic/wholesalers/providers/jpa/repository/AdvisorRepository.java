package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.AdvisorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvisorRepository extends JpaRepository<AdvisorData, Long> {

}

