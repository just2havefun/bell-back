package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.SubDistributorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubDistributorRepository extends JpaRepository<SubDistributorData, String> {

   @Query("SELECT s " +
         "FROM SubDistributorData s " +
         "WHERE s.email = :email")
   Optional<SubDistributorData> readSubDistributorByEmail(@Param("email") String email);

   @Query("SELECT s " +
         "FROM SubDistributorData s " +
         "WHERE s.oldUserData.email = :email " +
         "AND s.name <> s.oldUserData.name")
   List<SubDistributorData> listSubDistributorsByLeaderEmail(@Param("email") String email);
}

