package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.providers.jpa.entity.OldUserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OldUserRepository extends JpaRepository<OldUserData, Long> {

   @Query("SELECT new org.belltronic.wholesalers.domain.entity.ApplicationUser" +
         "(ou.email," +
         " ou.password," +
         " ou.name," +
         " ou.admin) " +
         "FROM OldUserData ou " +
         "WHERE ou.email = :email")
   Optional<ApplicationUser> getApplicationUserByEmail(@Param("email") String email);

   Optional<OldUserData> getOldUserDataByEmail(String email);
}

