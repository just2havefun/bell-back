package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.CommissionHistoryData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommissionHistoryRepository extends JpaRepository<CommissionHistoryData, Integer> {

   @Query("SELECT c FROM CommissionHistoryData c WHERE c.employee.identification = :employeeId and c.year = :year and c.month = :month")
   Optional<CommissionHistoryData> getCommissionHistoryDataByPeriod(@Param("employeeId") Long employeeId,
                                                                    @Param("year") int year, @Param("month") int month);

   @Query("SELECT c FROM CommissionHistoryData c WHERE c.employee.identification = :employeeId")
   List<CommissionHistoryData> listCommissionHistoryDataByEmployee(@Param("employeeId") Long employeeId);
}

