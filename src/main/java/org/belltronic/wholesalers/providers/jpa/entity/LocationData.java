package org.belltronic.wholesalers.providers.jpa.entity;

import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "control_vendedores")
public class LocationData {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Integer id;

   @NotNull
   @Column(name = "cedula")
   private Long employeeIdentification;

   @NotNull
   @Column(name = "latitud")
   private Double latitude;

   @NotNull
   @Column(name = "longitud")
   private Double longitude;

   @Column
   private Timestamp created_at;

   @UpdateTimestamp
   @Column
   private Timestamp updated_at;

   public LocationData() {
      LocalDateTime localDateTime = LocalDateTime.now().minusHours(5);
      this.created_at = Timestamp.valueOf(localDateTime);
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public Long getEmployeeIdentification() {
      return employeeIdentification;
   }

   public void setEmployeeIdentification(Long employeeIdentification) {
      this.employeeIdentification = employeeIdentification;
   }

   public Double getLatitude() {
      return latitude;
   }

   public void setLatitude(Double latitude) {
      this.latitude = latitude;
   }

   public Double getLongitude() {
      return longitude;
   }

   public void setLongitude(Double longitude) {
      this.longitude = longitude;
   }

   public Timestamp getCreated_at() {
      return created_at;
   }

   public void setCreated_at(Timestamp created_at) {
      this.created_at = created_at;
   }

   public Timestamp getUpdated_at() {
      return updated_at;
   }

   public void setUpdated_at(Timestamp updated_at) {
      this.updated_at = updated_at;
   }
}
