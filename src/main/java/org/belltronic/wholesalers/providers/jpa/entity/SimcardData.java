package org.belltronic.wholesalers.providers.jpa.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "simcards")
@Setter
@Getter
@NoArgsConstructor
public class SimcardData {

   @Id
   @Column(name = "numero")
   private String number;

   @NotNull
   @Column(name = "ICC", unique = true)
   private String icc;

   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   @Column(name = "fecha_vencimiento")
   private Date dueDate;

   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   @Column(name = "fecha_entrega")
   private Date deliveryDate;

   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   @Column(name = "fecha_activacion")
   private Date activationDate;

   @Column(name = "tipo")
   private Integer type;

   @Column(name = "paquete")
   private Integer pack;

   @Column(name = "pack_order")
   private Integer packOrder;

   @OneToOne
   @JoinColumn(name = "employee_identification")
   private EmployeeData employeeData;

   @ManyToOne
   @JoinColumn(name = "nombre_subdistribuidor")
   private SubDistributorData subDistributorData;

   @OneToOne
   @JoinColumn(name = "numero")
   private FreeData freeData;

}
