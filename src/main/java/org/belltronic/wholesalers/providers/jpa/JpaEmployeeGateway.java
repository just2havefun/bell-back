package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.errors.DuplicatedException;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.EmployeeGateway;
import org.belltronic.wholesalers.providers.jpa.entity.ApplicationUserData;
import org.belltronic.wholesalers.providers.jpa.entity.EmployeeData;
import org.belltronic.wholesalers.providers.jpa.repository.EmployeeRepository;
import org.belltronic.wholesalers.providers.jpa.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JpaEmployeeGateway implements EmployeeGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   @Autowired
   private EmployeeRepository employeeRepository;
   @Autowired
   private UserRepository userRepository;

   @Override
   public Employee createEmployee(Employee employeeDTO) {
      Optional<EmployeeData> employeeOpt = employeeRepository.findById(employeeDTO.getIdentification());
      if (!employeeOpt.isPresent()) {
         EmployeeData employeeData = modelMapper.map(employeeDTO, EmployeeData.class);
         EmployeeData storedEmployee = employeeRepository.save(employeeData);
         return modelMapper.map(storedEmployee, Employee.class);
      } else {
         throw new DuplicatedException();
      }
   }

   @Override
   public Employee readEmployee(Long identification) {
      EmployeeData employeeData = employeeRepository.findById(identification).orElseThrow(NotFoundException::new);
      return modelMapper.map(employeeData, Employee.class);
   }

   @Override
   public Employee updateEmployee(Long identification, Employee employeeDTO) {
      employeeRepository.findById(employeeDTO.getIdentification()).orElseThrow(NotFoundException::new);
      EmployeeData employeeData = modelMapper.map(employeeDTO, EmployeeData.class);
      EmployeeData storedEmployee = employeeRepository.save(employeeData);
      return modelMapper.map(storedEmployee, Employee.class);
   }

   @Override
   public void deleteEmployee(Long identification) {
      employeeRepository.deleteById(identification);
   }

   @Override
   public List<Employee> listEmployees() {
      return employeeRepository.findAll().stream()
            .map(employee -> modelMapper.map(employee, Employee.class))
            .collect(Collectors.toList());
   }

   @Override
   public List<Employee> listEmployeesByRole(Employee.Role role) {
      return employeeRepository.listEmployeeByRole(role).stream()
            .map(employee -> modelMapper.map(employee, Employee.class))
            .collect(Collectors.toList());
   }

   @Override
   public List<Employee> listEmployeesByRoleAndLeader(Employee.Role role, String leaderName) {
      return employeeRepository.listEmployeesByRoleAndLeaderName(role, leaderName).stream()
            .map(employee -> {
               Employee storedEmployee = modelMapper.map(employee, Employee.class);
               ApplicationUserData user = userRepository
                     .findByEmployeeIdentification(storedEmployee.getIdentification())
                     .orElseGet(ApplicationUserData::new);
               storedEmployee.setUser(modelMapper.map(user, ApplicationUser.class));
               return storedEmployee;
            })
            .collect(Collectors.toList());
   }
}
