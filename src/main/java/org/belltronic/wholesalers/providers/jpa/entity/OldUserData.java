package org.belltronic.wholesalers.providers.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class OldUserData {

   @Column(name = "identification")
   private Long identification;

   @Id
   @Column(name = "email")
   private String email;

   @Column(name = "password")
   private String password;

   @Column(name = "name")
   private String name;

   @Column(name = "isAdmin")
   private boolean admin;

   public OldUserData() {
      /* Empty constructor required by JPA */
   }

   public Long getIdentification() {
      return identification;
   }

   public void setIdentification(Long identification) {
      this.identification = identification;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public boolean isAdmin() {
      return admin;
   }

   public void setAdmin(boolean admin) {
      this.admin = admin;
   }
}
