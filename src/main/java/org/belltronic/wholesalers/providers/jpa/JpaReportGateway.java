package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.CommissionHistory;
import org.belltronic.wholesalers.domain.entity.report.csv.EmployeeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.FreeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.ReportGateway;
import org.belltronic.wholesalers.providers.jpa.entity.CommissionHistoryData;
import org.belltronic.wholesalers.providers.jpa.repository.CommissionHistoryRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SimcardRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JpaReportGateway implements ReportGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   private SimcardRepository simcardRepository;
   private CommissionHistoryRepository commissionHistoryRepository;

   @Autowired
   public JpaReportGateway(SimcardRepository simcardRepository, CommissionHistoryRepository commissionHistoryRepository) {
      this.simcardRepository = simcardRepository;
      this.commissionHistoryRepository = commissionHistoryRepository;
   }

   @Override
   public List<SimcardReport> listSimcardsByTypeAndOwnerAndDate(final int type, final String ownerName, final Date initialDate, final Date finalDate) {
      return simcardRepository.listSimcardsReportByTypeAndLeaderName(type, ownerName, initialDate, finalDate);
   }

   @Override
   public List<SimcardReport> listSimcardsByTypeAndDate(final int type, final Date initialDate, final Date finalDate) {
      return simcardRepository.listSimcardsReportByType(type, initialDate, finalDate);
   }

   @Override
   public List<EmployeeReport> listEmployeeSimcards(Long identification) {
      return simcardRepository.listSimcardsReportByEmployee(identification);
   }

   @Override
   public CommissionHistory getCommissionHistoryByPeriod(Long employeeIdentification, Integer year, Integer month) {
      CommissionHistoryData commissionHistoryData = commissionHistoryRepository
            .getCommissionHistoryDataByPeriod(employeeIdentification, year, month).orElseThrow(NotFoundException::new);

      return modelMapper.map(commissionHistoryData, CommissionHistory.class);
   }

   @Override
   public CommissionHistory createCommissionHistory(CommissionHistory commissionHistory) {
      Optional<CommissionHistoryData> commissionHistoryDataOpt = commissionHistoryRepository
            .getCommissionHistoryDataByPeriod(commissionHistory.getEmployeeIdentification(), commissionHistory.getYear(), commissionHistory.getMonth());

      commissionHistoryDataOpt.ifPresent(commissionHistoryData -> commissionHistoryRepository.deleteById(commissionHistoryData.getId()));

      CommissionHistoryData commissionHistoryData = modelMapper.map(commissionHistory, CommissionHistoryData.class);
      CommissionHistoryData storedCommissionHistoryData = commissionHistoryRepository.save(commissionHistoryData);
      return modelMapper.map(storedCommissionHistoryData, CommissionHistory.class);
   }

   @Override
   public void deleteCommissionHistory(int id) {
      commissionHistoryRepository.deleteById(id);
   }

   @Override
   public List<CommissionHistory> listCommissionHistoryByEmployee(Long employeeIdentification) {
      return commissionHistoryRepository.listCommissionHistoryDataByEmployee(employeeIdentification)
            .stream()
            .map((commissionHistoryData) -> modelMapper.map(commissionHistoryData, CommissionHistory.class))
            .collect(Collectors.toList());
   }

}
