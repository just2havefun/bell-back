package org.belltronic.wholesalers.providers.jpa;

import org.apache.commons.lang.RandomStringUtils;
import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.errors.DuplicatedException;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.belltronic.wholesalers.providers.jpa.entity.ApplicationUserData;
import org.belltronic.wholesalers.providers.jpa.entity.EmployeeData;
import org.belltronic.wholesalers.providers.jpa.entity.SubDistributorData;
import org.belltronic.wholesalers.providers.jpa.repository.EmployeeRepository;
import org.belltronic.wholesalers.providers.jpa.repository.OldUserRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SubDistributorRepository;
import org.belltronic.wholesalers.providers.jpa.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class JpaUserGateway implements UserGateway {

   private static final ModelMapper modelMapper = new ModelMapper();
   @Autowired
   private UserRepository userRepository;
   @Autowired
   private OldUserRepository oldUserRepository;
   @Autowired
   private SubDistributorRepository subDistributorRepository;
   @Autowired
   private EmployeeRepository employeeRepository;

   @Override
   public ApplicationUser createUser(ApplicationUser applicationUser) {
      EmployeeData employeeData = employeeRepository.findById(applicationUser.getEmployee().getIdentification())
            .orElseThrow(NotFoundException::new);

      String password = RandomStringUtils.random(10, true, true);

      ApplicationUserData storedUser = userRepository.findByEmployeeIdentification(employeeData.getIdentification())
            .map((user) -> {
               if (!user.getEmail().equals(applicationUser.getEmail())) {
                  userRepository.delete(user);
                  userRepository.flush();
                  ApplicationUserData newUser = modelMapper.map(applicationUser, ApplicationUserData.class);
                  newUser.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
                  return userRepository.save(newUser);
               } else {
                  return user;
               }
            })
            .orElseGet(() -> {
               ApplicationUserData applicationUserData = modelMapper.map(applicationUser, ApplicationUserData.class);
               applicationUserData.setEmployee(employeeData);
               applicationUserData.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
               return userRepository.save(applicationUserData);
            });

      ApplicationUser storedApplicationUser = modelMapper.map(storedUser, ApplicationUser.class);
      storedApplicationUser.setPassword(password);
      return storedApplicationUser;
   }

   @Override
   public ApplicationUser readUser(String email) {
      ApplicationUser user;
      try {
         user = oldUserRepository.getApplicationUserByEmail(email).orElseThrow(NotFoundException::new);
         user.setType(ApplicationUser.Type.SUBDISTRIBUTOR);
      } catch (NotFoundException ie) {
         try {
            SubDistributorData subDistributorData = subDistributorRepository.readSubDistributorByEmail(email).orElseThrow(NotFoundException::new);
            user = new ApplicationUser(subDistributorData.getEmail(), subDistributorData.getPassword(), subDistributorData.getName(), false);
            user.setType(ApplicationUser.Type.SUBDISTRIBUTOR);
         } catch (NotFoundException e) {
            ApplicationUserData applicationUserData = userRepository.findById(email).orElseThrow(NotFoundException::new);
            user = modelMapper.map(applicationUserData, ApplicationUser.class);
         }
      }
      return user;
   }

   @Override
   public ApplicationUser updateUser(String email, ApplicationUser applicationUser) {
      if (applicationUser.getType() == ApplicationUser.Type.SUBDISTRIBUTOR) {
         SubDistributorData subDistributorData = subDistributorRepository
               .readSubDistributorByEmail(applicationUser.getEmail()).orElseThrow(NotFoundException::new);
         subDistributorData.setPassword(BCrypt.hashpw(applicationUser.getPassword(), BCrypt.gensalt()));
         subDistributorRepository.save(subDistributorData);
         ApplicationUser updatedUser = new ApplicationUser(applicationUser.getEmail(), applicationUser.getPassword(),
               subDistributorData.getName(), applicationUser.isAdmin());
         updatedUser.setType(applicationUser.getType());
         updatedUser.setRole(applicationUser.getRole());
         return updatedUser;
      } else {
         ApplicationUserData storedApplicationUser = userRepository.findById(email).orElseThrow(NotFoundException::new);

         if (storedApplicationUser.getPassword() == null) {
            applicationUser.setPassword(storedApplicationUser.getPassword());
         }

         if (!email.equals(storedApplicationUser.getEmail()) && userRepository.existsById(storedApplicationUser.getEmail())) {
            throw new DuplicatedException();
         }

         if (!email.equals(applicationUser.getEmail())) {
            if (!userRepository.existsById(applicationUser.getEmail())) {
               userRepository.deleteById(storedApplicationUser.getEmail());
            }
         }

         ApplicationUserData userToBeUpdated = modelMapper.map(applicationUser, ApplicationUserData.class);
         userToBeUpdated.setPassword(BCrypt.hashpw(userToBeUpdated.getPassword(), BCrypt.gensalt()));
         ApplicationUserData updatedApplicationUser = userRepository.save(userToBeUpdated);
         return modelMapper.map(updatedApplicationUser, ApplicationUser.class);
      }
   }

   @Override
   public void deleteUser(String email) {
      userRepository.deleteById(email);
   }

   @Override
   public void deleteUserByEmployee(Long identification) {
      userRepository.findByEmployeeIdentification(identification)
            .ifPresent(applicationUserData -> userRepository.deleteById(applicationUserData.getEmail()));
   }

   @Override
   public ApplicationUser readUserByEmployee(Long identification) {
      ApplicationUserData applicationUserData = userRepository.findByEmployeeIdentification(identification).orElseThrow(NotFoundException::new);
      return modelMapper.map(applicationUserData, ApplicationUser.class);
   }
}
