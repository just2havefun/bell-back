package org.belltronic.wholesalers.providers.jpa.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.belltronic.wholesalers.domain.entity.ApplicationUser;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
@NoArgsConstructor
@Getter
@Setter
public class ApplicationUserData {

   @Id
   @Column(name = "email")
   private String email;

   @Column(name = "password")
   private String password;

   @NotNull
   @Column(name = "type")
   private ApplicationUser.Type type;

   @NotNull
   @Column(name = "role")
   private ApplicationUser.Role role;

   @OneToOne
   @JoinColumn(name = "employee_identification")
   private EmployeeData employee;

}
