package org.belltronic.wholesalers.providers.jpa;

import org.belltronic.wholesalers.domain.entity.Simcard;
import org.belltronic.wholesalers.domain.errors.DatabaseException;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.SimcardGateway;
import org.belltronic.wholesalers.providers.jpa.entity.EmployeeData;
import org.belltronic.wholesalers.providers.jpa.entity.FreeData;
import org.belltronic.wholesalers.providers.jpa.entity.SimcardData;
import org.belltronic.wholesalers.providers.jpa.entity.SubDistributorData;
import org.belltronic.wholesalers.providers.jpa.repository.EmployeeRepository;
import org.belltronic.wholesalers.providers.jpa.repository.FreeRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SimcardRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SubDistributorRepository;
import org.joda.time.LocalDate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class JpaSimcardGateway implements SimcardGateway {

   private static final ModelMapper modelMapper = new ModelMapper();

   @Autowired
   private SimcardRepository simcardRepository;
   @Autowired
   private SubDistributorRepository subDistributorRepository;
   @Autowired
   private FreeRepository freeRepository;
   @Autowired
   private EmployeeRepository employeeRepository;

   @PersistenceContext
   private EntityManager manager;

   @Override
   public Simcard readSimcard(String id) {
      SimcardData simcardData = simcardRepository.findById(id)
            .orElseGet(() -> simcardRepository.getSimcardByIcc(id).orElseThrow(NotFoundException::new));
      Simcard simcard = modelMapper.map(simcardData, Simcard.class);
      if (simcardData.getSubDistributorData() != null) {
         simcard.setSubDistributorName(simcardData.getSubDistributorData().getName());
      }
      simcard = setFreeData(simcard);
      return simcard;
   }

   @Override
   public Simcard updateSimcard(Simcard simcard) {
      SimcardData simcardData = modelMapper.map(simcard, SimcardData.class);
      if (simcard.getEmployeeIdentification() != null) {
         EmployeeData employeeData = employeeRepository.findById(simcard.getEmployeeIdentification())
               .orElseThrow(NotFoundException::new);
         simcardData.setEmployeeData(employeeData);
      } else {
         simcardData.setEmployeeData(null);
         simcardData.setPack(null);
      }
      SubDistributorData subDistributorData = subDistributorRepository.findById(simcard.getSubDistributorName())
            .orElseThrow(NotFoundException::new);
      simcardData.setSubDistributorData(subDistributorData);
      SimcardData storedSimcard = simcardRepository.save(simcardData);
      cleanFreeData(storedSimcard);
      return modelMapper.map(storedSimcard, Simcard.class);
   }

   @Override
   public Simcard packSimcard(Simcard simcard) {
      SimcardData simcardData = simcardRepository.findById(simcard.getNumber()).orElseThrow(NotFoundException::new);
      simcardData.setPack(simcard.getPack());
      simcardData.setPackOrder(simcard.getPackOrder());
      SimcardData storedSimcard = simcardRepository.save(simcardData);
      cleanFreeData(storedSimcard);
      return modelMapper.map(storedSimcard, Simcard.class);
   }

   @Override
   public List<Simcard> listSimcardsByTypeAndOwner(Integer type, String ownerName) {
      return simcardRepository.listSimcardsByTypeAndLeaderName(type, ownerName)
            .stream()
            .map(simcardData -> {
               Simcard simcard = modelMapper.map(simcardData, Simcard.class);
               return setFreeData(simcard);
            }).collect(Collectors.toList());
   }

   @Override
   public List<Simcard> getPack(int packNumber) {
      return simcardRepository.findAllByPackOrderByPackOrder(packNumber)
            .stream()
            .map(simcardData -> {
               Simcard simcard = modelMapper.map(simcardData, Simcard.class);
               return setFreeData(simcard);
            }).collect(Collectors.toList());
   }

   @Override
   public int getLastPack() {
      return simcardRepository.findFirstByOrderByPackDesc().map(SimcardData::getPack)
            .orElseThrow(NotFoundException::new);
   }

   @Override
   public void assignPackToEmployee(int packNumber, long employeeIdentification) {
      EmployeeData employee = employeeRepository.findById(employeeIdentification)
            .orElseThrow(NotFoundException::new);
      SubDistributorData subDistributor = subDistributorRepository.findById(employee.getLeaderName())
            .orElseThrow(NotFoundException::new);
      List<SimcardData> simcards = simcardRepository.findAllByPack(packNumber);
      simcards.forEach((simcard) -> {
         simcard.setEmployeeData(employee);
         simcard.setSubDistributorData(subDistributor);
         simcard.setDeliveryDate(new LocalDate().toDate());
         simcardRepository.save(simcard);
      });
   }

   @Override
   public void unassignPack(int packNumber) {
      List<SimcardData> simcards = simcardRepository.findAllByPack(packNumber);
      SubDistributorData subDistributorData = subDistributorRepository.findById("OFICINA")
            .orElseThrow(NotFoundException::new);
      simcards.forEach((simcard) -> {
         simcard.setSubDistributorData(subDistributorData);
         simcard.setEmployeeData(null);
         simcardRepository.save(simcard);
      });
   }

   @Override
   @Transactional
   public void updateNumberByIcc(String number, String icc) {
      Optional<SimcardData> simcardDataOpt = simcardRepository.findById(number);
      if (simcardDataOpt.isPresent()) {
         if (!simcardDataOpt.get().getIcc().equals(icc)) {
            simcardRepository.delete(simcardDataOpt.get());
         }
      }
      int rowsAffected = manager.createNativeQuery("update simcards set numero = ? where icc = ?")
            .setParameter(1, number)
            .setParameter(2, icc)
            .executeUpdate();

      if (rowsAffected == 0) {
         throw new DatabaseException();
      }
   }

   @Override
   @Transactional
   public void deleteConflictedNumberByIcc(String number, String icc) {
      simcardRepository.deleteConflictedNumberByIcc(number, icc);
   }

   private Simcard setFreeData(Simcard simcard) {
      if (simcard.getType() == 2) {
         Optional<FreeData> free = freeRepository.findById(simcard.getNumber());
         if (free.isPresent()) {
            simcard.setPlan(free.get().getPlan());
            simcard.setValue(free.get().getValue());
         }
      }
      return simcard;
   }

   private void cleanFreeData(SimcardData simcardData) {
      if (simcardData.getType() == 1) {
         if (freeRepository.findById(simcardData.getNumber()).isPresent()) {
            freeRepository.deleteById(simcardData.getNumber());
         }
      }
   }
}
