package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.CommissionData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Repository
public interface CommissionRepository extends JpaRepository<CommissionData, Long> {

   @Query(value = "SELECT e.identification, e.name, c.periodo, " +
         "COALESCE(SUM(CASE WHEN s.tipo = 1 THEN c.valor END),0) prepaidCommission, " +
         "COALESCE(SUM(CASE WHEN s.tipo = 2 THEN c.valor END),0) freeCommission " +
         "FROM comisiones c " +
         "INNER JOIN simcards s ON c.telefono = s.numero " +
         "INNER JOIN employee e ON s.employee_identification = e.identification " +
         "WHERE e.leader_name = :name " +
         "AND c.periodo = :period " +
         "AND DATE_ADD(s.fecha_activacion, INTERVAL 8 MONTH) > :dueDate " +
         "GROUP BY e.identification", nativeQuery = true)
   List<Object[]> getSubDistributorCommissionsReport(@Param("name") String name, @Param("period") String period,
                                                     @Param("dueDate") Date dueDate);

   @Query(value = "SELECT DISTINCT c.periodo FROM comisiones c "
         + "INNER JOIN simcards s ON c.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.identification = :identification", nativeQuery = true)
   Set<Object> getAvailablePeriods(@Param("identification") Long identification);

   @Query(value = "SELECT DISTINCT c.periodo FROM comisiones c "
         + "INNER JOIN simcards s ON c.telefono = s.numero "
         + "INNER JOIN employee e ON s.employee_identification = e.identification "
         + "WHERE e.leader_name = :name", nativeQuery = true)
   Set<Object> getSubAvailablePeriods(@Param("name") String name);

}

