package org.belltronic.wholesalers.providers.jpa.repository;

import org.belltronic.wholesalers.providers.jpa.entity.ApplicationUserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<ApplicationUserData, String> {

   @Query("SELECT u FROM ApplicationUserData u WHERE u.employee.identification = :identification")
   Optional<ApplicationUserData> findByEmployeeIdentification(@Param("identification") Long identification);
}

