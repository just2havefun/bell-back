package org.belltronic.wholesalers.providers.mailproviders.spring;

import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.errors.ErrorResponse;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.gateway.MailGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class SendEmailProvider implements MailGateway {

   @Value("${gmail.username}")
   private String username;
   @Value("${gmail.password}")
   private String password;

   private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
   private static Properties props = new Properties();

   static {
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", "smtp.gmail.com");
      props.put("mail.smtp.port", "587");
   }

   @Override
   @Async
   public void sendEmail(MailMessage mailMessage) {
      try {
         LOGGER.info("Sending Email to: " + mailMessage.getTo());
         Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(username, password);
            }
         });

         MimeMessage msg = new MimeMessage(session);
         Multipart multipart = new MimeMultipart();

         MimeBodyPart html = new MimeBodyPart();
         html.setContent(mailMessage.getContent(), mailMessage.getContentType());
         multipart.addBodyPart(html);

         if (!mailMessage.getAttachments().isEmpty()) {
            for (File messageAttachment : mailMessage.getAttachments()) {
               MimeBodyPart attachment = new MimeBodyPart();
               attachment.attachFile(messageAttachment);
               multipart.addBodyPart(attachment);
            }
         }
         msg.setFrom(new InternetAddress(username, false));
         msg.setRecipients(Message.RecipientType.TO, mailMessage.getTo());
         msg.setSubject(mailMessage.getSubject());
         msg.setContent(multipart);
         msg.setSentDate(new Date());
         msg.saveChanges();

         Transport.send(msg);
         LOGGER.info("Email Sent to: " + mailMessage.getTo());
      } catch (MessagingException | IOException e) {
         LOGGER.error("Error Sending Email: " + e.getMessage());
         ErrorResponse.ErrorItem<String> errorItem = new ErrorResponse.ErrorItem<>();
         errorItem.setObject(mailMessage.getTo());
         errorItem.setMessage(e.getMessage());
         throw new InternalException(errorItem, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }
}
