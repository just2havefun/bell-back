package org.belltronic.wholesalers.entrypoints.rest.location;

import org.belltronic.wholesalers.domain.entity.Location;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.location.CrudLocationUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class LocationEndpoint {

   public static final String API_PATH = "/location";

   private static final Logger LOGGER = LoggerFactory.getLogger(LocationEndpoint.class);

   @Autowired
   private CrudLocationUseCase createLocationUseCase;

   @RequestMapping(value = API_PATH, method = POST)
   public Location createLocation(@RequestBody Location locationDTO) throws InternalException {
      LOGGER.info("Storing location for employee: {}", locationDTO.getEmployeeIdentification());
      return createLocationUseCase.createLocation(locationDTO);
   }

}
