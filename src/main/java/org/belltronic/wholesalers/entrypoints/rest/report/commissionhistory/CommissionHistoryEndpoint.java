package org.belltronic.wholesalers.entrypoints.rest.report.commissionhistory;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.belltronic.wholesalers.domain.entity.CommissionHistory;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.report.commission.CrudCommissionHistoryUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class CommissionHistoryEndpoint {

   private static final String API_PATH = "/v1/commissionHistory";

   private static final Logger LOGGER = LoggerFactory.getLogger(CommissionHistoryEndpoint.class);

   @Autowired
   private CrudCommissionHistoryUseCase crudCommissionHistoryUseCase;

   @RequestMapping(value = API_PATH + "/{identification}/{period}", method = GET)
   public CommissionHistory getCommissionsHistoryByPeriod(@PathVariable("identification") Long identification,
                                                          @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period)
         throws InternalException {
      LOGGER.info("Retrieving commission for employee {} of the period {}", identification, period);
      String[] date = period.split("-");
      return crudCommissionHistoryUseCase
            .getCommissionHistoryByPeriod(identification, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
   }

   @RequestMapping(value = API_PATH, method = POST)
   public CommissionHistory createCommissionHistory(@RequestBody CommissionHistory commissionHistory) {
      LOGGER.info("Creating commissions history for wholesaler {}", commissionHistory.getEmployeeIdentification());
      return crudCommissionHistoryUseCase.createCommissionHistory(commissionHistory);
   }

   @RequestMapping(value = API_PATH + "/{id}", method = DELETE)
   public void deleteCommissionHistory(@PathVariable("id") int id)
         throws InternalException {
      LOGGER.info("Deleting commissions with id {} ", id);
      crudCommissionHistoryUseCase.deleteCommissionHistory(id);
   }

   @RequestMapping(value = API_PATH + "/{identification}", method = GET)
   public List<CommissionHistory> listCommissionsHistoryByEmployee(@PathVariable("identification") Long identification)
         throws InternalException {
      LOGGER.info("Retrieving all commissions for employee {}", identification);
      return crudCommissionHistoryUseCase.listCommissionHistoryByEmployee(identification);
   }
}
