package org.belltronic.wholesalers.entrypoints.rest.employee;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.usecase.employee.CrudEmployeeUseCase;
import org.belltronic.wholesalers.domain.usecase.employee.ListEmployeesUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
public class EmployeeEndpoint {

   public static final String API_PATH = "/v1/employee";

   private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeEndpoint.class);

   @Autowired
   private CrudEmployeeUseCase crudEmployeeUseCase;
   @Autowired
   private ListEmployeesUseCase listEmployeesUseCase;

   @RequestMapping(value = API_PATH, method = POST)
   public Employee createEmployee(@RequestBody Employee employeeDTO) {
      LOGGER.info("Creating employee with identification: {}", employeeDTO.getIdentification());
      return crudEmployeeUseCase.createEmployee(employeeDTO);
   }

   @RequestMapping(value = API_PATH + "/{identification}", method = GET)
   public Employee readEmployee(@PathVariable("identification") Long identification) {
      LOGGER.info("Retrieving employee for identification: {}", identification);
      return crudEmployeeUseCase.readEmployee(identification);
   }

   @RequestMapping(value = API_PATH + "/{identification}", method = PUT)
   public Employee updateEmployee(@PathVariable("identification") Long identification,
                                  @RequestBody @Valid Employee employeeDTO) {
      LOGGER.info("Update employee with identification: {}", identification);
      return crudEmployeeUseCase.updateEmployee(identification, employeeDTO);
   }

   @RequestMapping(value = API_PATH + "/{identification}", method = DELETE)
   public void deleteEmployee(@PathVariable("identification") Long identification) {
      LOGGER.info("Deleting employee for identification: {}", identification);
      crudEmployeeUseCase.deleteEmployee(identification);
   }

   @RequestMapping(value = API_PATH, method = GET)
   public List<Employee> listEmployees() {
      LOGGER.info("Retrieving all the employees");
      return listEmployeesUseCase.listEmployees();
   }

   @RequestMapping(value = API_PATH + "/wholesalers", method = GET)
   public List<Employee> listWholesalers() {
      LOGGER.info("Retrieving all the wholesalers");
      return listEmployeesUseCase.listWholesalers();
   }

   @RequestMapping(value = API_PATH + "/wholesalers/{leaderName}", method = GET)
   public List<Employee> listWholesalersByLeader(@PathVariable("leaderName") String leaderName) {
      LOGGER.info("Retrieving all the wholesalers for sub: {}", leaderName);
      return listEmployeesUseCase.listWholesalersByLeader(leaderName);
   }
}
