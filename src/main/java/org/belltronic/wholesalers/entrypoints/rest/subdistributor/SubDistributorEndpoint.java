package org.belltronic.wholesalers.entrypoints.rest.subdistributor;

import org.belltronic.wholesalers.domain.entity.SubDistributor;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.subdistributor.CrudSubDistributorUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
public class SubDistributorEndpoint {

   public static final String API_PATH = "/v1/subdistributor";

   private static final Logger LOGGER = LoggerFactory.getLogger(SubDistributorEndpoint.class);

   @Autowired
   private CrudSubDistributorUseCase crudSubDistributorUseCase;

   @RequestMapping(value = API_PATH + "/{name}", method = GET)
   public SubDistributor readSubDistributor(@PathVariable("name") String name) throws InternalException {
      LOGGER.info("Retrieving subDistributor with name: {}", name);
      return crudSubDistributorUseCase.readSubDistributor(name);
   }

   @RequestMapping(value = API_PATH + "/email/{email:.+}", method = GET)
   public SubDistributor readSubDistributorByEmail(@PathVariable("email") String email) throws InternalException {
      LOGGER.info("Retrieving subDistributor with email: {}", email);
      return crudSubDistributorUseCase.readSubdistributorByEmail(email);
   }

   @RequestMapping(value = API_PATH, method = GET)
   public List<SubDistributor> listSubdistributors() {
      LOGGER.info("Retrieving all the subDistributors");
      return crudSubDistributorUseCase.listSubDistributors();
   }

   @RequestMapping(value = API_PATH, method = PUT)
   public SubDistributor updateSubdistributors(@RequestBody SubDistributor subDistributor) {
      LOGGER.info("Updating subDistributor");
      return crudSubDistributorUseCase.updateSubDistributor(subDistributor);
   }
}
