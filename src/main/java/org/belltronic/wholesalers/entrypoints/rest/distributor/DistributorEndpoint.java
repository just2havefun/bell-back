package org.belltronic.wholesalers.entrypoints.rest.distributor;

import org.belltronic.wholesalers.domain.entity.Distributor;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.distributor.CrudDistributorUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class DistributorEndpoint {

   public static final String API_PATH = "/v1/distributor";

   private static final Logger LOGGER = LoggerFactory.getLogger(DistributorEndpoint.class);

   @Autowired
   private CrudDistributorUseCase crudDistributorUseCase;

   @RequestMapping(value = API_PATH + "/email/{email:.+}", method = GET)
   public Distributor readDistributorByEmail(@PathVariable("email") String email) throws InternalException {
      LOGGER.info("Retrieving Distributor with email: {}", email);
      return crudDistributorUseCase.readDistributorByEmail(email);
   }

   @RequestMapping(value = API_PATH, method = GET)
   public List<Distributor> listDistributors() {
      LOGGER.info("Retrieving all the subDistributors");
      return crudDistributorUseCase.listDistributors();
   }
}
