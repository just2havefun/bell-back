package org.belltronic.wholesalers.entrypoints.rest.simcard;

import org.belltronic.wholesalers.domain.entity.Simcard;
import org.belltronic.wholesalers.domain.usecase.simcard.CrudSimcardUseCase;
import org.belltronic.wholesalers.domain.usecase.simcard.PackSimcardUseCase;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
public class SimcardEndpoint {

   public static final String API_PATH = "/v1/simcard";

   private static final Logger LOGGER = LoggerFactory.getLogger(SimcardEndpoint.class);

   @Autowired
   private CrudSimcardUseCase crudSimcardUseCase;
   @Autowired
   private PackSimcardUseCase packSimcardUseCase;

   @RequestMapping(value = API_PATH + "/{id}", method = GET)
   public Simcard readSimcard(@PathVariable("id") String id) {
      LOGGER.info("Retrieving simcard for id: {}", id);
      return crudSimcardUseCase.readSimcard(id);
   }

   @RequestMapping(value = API_PATH + "/assign/{id}", method = PUT)
   public Simcard assignSimcard(@PathVariable("id") String id, @RequestBody Simcard simcard) {
      LOGGER.info("Assigning simcard for id: {} to employee {}", id, simcard.getEmployeeIdentification());
      LocalDate now = LocalDate.now();
      simcard.setDeliveryDate(now.toDate());
      return crudSimcardUseCase.updateSimcard(id, simcard);
   }

   @RequestMapping(value = API_PATH + "/unassign/{id}", method = PUT)
   public Simcard unassignSimcard(@PathVariable("id") String id) {
      LOGGER.info("Unassigning simcard for id: {}", id);
      Simcard simcard = crudSimcardUseCase.readSimcard(id);
      simcard.setEmployeeIdentification(null);
      return crudSimcardUseCase.updateSimcard(id, simcard);
   }

   @RequestMapping(value = API_PATH + "/pack/{packNumber}", method = GET)
   public List<Simcard> readPack(@PathVariable("packNumber") int packNumber) {
      LOGGER.info("Getting Pack{}", packNumber);
      return packSimcardUseCase.getPack(packNumber);
   }

   @RequestMapping(value = API_PATH + "/pack", method = POST)
   public int packSimcards(@RequestBody List<Simcard> simcards) {
      LOGGER.info("Packing {} simcards", simcards.size());
      return packSimcardUseCase.packSimcards(simcards);
   }

   @RequestMapping(value = API_PATH + "/pack/{packNumber}", method = PUT)
   public List<Simcard> updatePack(@PathVariable("packNumber") int packNumber, @RequestBody List<Simcard> simcards) {
      LOGGER.info("Updating Pack {}", packNumber);
      return packSimcardUseCase.updatePack(packNumber, simcards);
   }

   @RequestMapping(value = API_PATH + "/assign/pack/{packNumber}/{employeeIdentification}", method = PUT)
   public void assignPack(@PathVariable("packNumber") int packNumber, @PathVariable("employeeIdentification") Long employeeIdentification) {
      LOGGER.info("Assigning pack {} to {}", packNumber, employeeIdentification);
      packSimcardUseCase.assignPackToEmployee(packNumber, employeeIdentification);
   }

   @RequestMapping(value = API_PATH + "/unassign/pack/{packNumber}", method = PUT)
   public void unassignPack(@PathVariable("packNumber") int packNumber) {
      LOGGER.info("Unassigning pack {}", packNumber);
      packSimcardUseCase.unassignPack(packNumber);
   }

   @RequestMapping(value = API_PATH + "/pack/clean", method = POST)
   public void cleanSimsPack(@RequestBody List<Simcard> simcards) {
      LOGGER.info("Removing pack numbers");
      packSimcardUseCase.removePackNumbers(simcards);
   }
}
