package org.belltronic.wholesalers.entrypoints.rest.report.simcard;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.usecase.report.simcard.GetAssignationReportUseCase;
import org.belltronic.wholesalers.domain.usecase.report.simcard.GetEmployeeReportUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class SimcardReportEndpoint {

   private static final String API_PATH = "/v1/report/simcard";

   private static final Logger LOGGER = LoggerFactory.getLogger(SimcardReportEndpoint.class);

   @Autowired
   private GetAssignationReportUseCase getAssignationReportUseCase;
   @Autowired
   private GetEmployeeReportUseCase getEmployeeReportUseCase;

   @RequestMapping(value = API_PATH + "/assignation", method = POST)
   public ResponseEntity<Resource> getAssignationBySubDistributor(@RequestBody AsignationReportRequest request) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
      LOGGER.info("Retrieving all simcards for subdistributor {}", request.getSubDistributorName());
      File file = getAssignationReportUseCase.getReport(request.getType(), request.getSubDistributorName(), request.getInitialDate(), request.getFinalDate());
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=simcards.csv");
      return ResponseEntity.ok()
            .contentLength(file.length())
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(resource);
   }

   @RequestMapping(value = API_PATH + "/employee/{identification}", method = GET)
   public ResponseEntity<Resource> getAssignationByEmployee(@PathVariable("identification") Long identification) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
      LOGGER.info("Retrieving all simcards for employee {}", identification);
      File file = getEmployeeReportUseCase.getEmployeeReport(identification);
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=simcards.csv");
      return ResponseEntity.ok()
            .contentLength(file.length())
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(resource);
   }

}
