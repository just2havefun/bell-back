package org.belltronic.wholesalers.entrypoints.rest.report.production;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.belltronic.wholesalers.domain.entity.report.ProductionReport;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.report.production.GetProductionReportUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class ProductionEndpoint {

   private static final String API_PATH = "/v1/production";

   private static final Logger LOGGER = LoggerFactory.getLogger(ProductionEndpoint.class);

   @Autowired
   private GetProductionReportUseCase getProductionReportUseCase;

   @RequestMapping(value = API_PATH + "/period/{name}", method = GET)
   public Set<String> getAvailablePeriods(@PathVariable("name") String name)
         throws InternalException {
      LOGGER.info("Retrieving production periods for employee {} ", name);
      return getProductionReportUseCase.getProductionAvailablePeriodsUseCase(name);
   }

   @RequestMapping(value = API_PATH + "/sub/{name}/{period}", method = GET)
   public List<ProductionReport> getSubDistributorProductionByPeriod(@PathVariable("name") String name,
                                                                     @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period) throws ParseException {
      LOGGER.info("Retrieving production for subdistributor {} of the period {}", name, period);
      return getProductionReportUseCase.getProductionReport(name, period);
   }

}
