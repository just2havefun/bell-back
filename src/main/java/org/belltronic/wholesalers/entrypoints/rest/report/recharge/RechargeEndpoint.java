package org.belltronic.wholesalers.entrypoints.rest.report.recharge;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.belltronic.wholesalers.domain.entity.report.RechargeReport;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.report.recharge.GetRechargeReportUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class RechargeEndpoint {

   private static final String API_PATH = "/v1/recharge";

   private static final Logger LOGGER = LoggerFactory.getLogger(RechargeEndpoint.class);

   @Autowired
   private GetRechargeReportUseCase getRechargeReportUseCase;

   @RequestMapping(value = API_PATH + "/{subdistributor}/{period}", method = GET)
   public List<RechargeReport> getRechargesByPeriod(@PathVariable("subdistributor") String subdistributor,
                                                    @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period)
         throws InternalException, ParseException {
      LOGGER.info("Retrieving recharges for sub {} of the period {}", subdistributor, period);
      String[] date = period.split("-");
      return getRechargeReportUseCase
            .getSubRechargeReport(subdistributor, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
   }

}
