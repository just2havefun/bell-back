package org.belltronic.wholesalers.entrypoints.rest.file;

import org.belltronic.wholesalers.crosscutting.util.FileLoader;
import org.belltronic.wholesalers.domain.errors.ErrorResponse;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.file.updateNumbersUseCase.UpdateNumbersFileUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping("/v1//upload")
public class FileEndpoint {

   private static final Logger LOGGER = LoggerFactory.getLogger(FileEndpoint.class);

   @Autowired
   private UpdateNumbersFileUseCase updateNumbersFileUseCase;

   @PostMapping(value = "/numbers-icc")
   public void uploadNumbersByIcc(@RequestParam("file") MultipartFile multipartFile,
                                  @RequestParam("receiver") String receiver) throws InternalException {
      LOGGER.info("Updating numbers by Icc");
      try {
         File file = FileLoader.convertMultiPartToFile(multipartFile);
         updateNumbersFileUseCase.updateNumbersByIcc(file, receiver);
      } catch (IOException e) {
         LOGGER.error("Error Reading file: " + multipartFile.getName(), e);
         ErrorResponse.ErrorItem<String> errorItem = new ErrorResponse.ErrorItem<>();
         errorItem.setObject(multipartFile.getName());
         errorItem.setMessage("Error leyendo el archivo");
         throw new InternalException(errorItem, HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

}
