package org.belltronic.wholesalers.entrypoints.rest.report.simcard;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AsignationReportRequest {

   private int type;
   private String subDistributorName;
   private Date initialDate;
   private Date finalDate;

}
