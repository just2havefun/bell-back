package org.belltronic.wholesalers.entrypoints.rest.report.commission;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.entity.Projection;
import org.belltronic.wholesalers.domain.entity.report.CommissionReport;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.report.commission.GetAvailablePeriodsUseCase;
import org.belltronic.wholesalers.domain.usecase.report.commission.GetCommissionReportUseCase;
import org.belltronic.wholesalers.domain.usecase.report.commission.GetProjectionUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class CommissionEndpoint {

   private static final String API_PATH = "/v1/commission";

   private static final Logger LOGGER = LoggerFactory.getLogger(CommissionEndpoint.class);

   @Autowired
   private GetCommissionReportUseCase getCommissionReportUseCase;
   @Autowired
   private GetAvailablePeriodsUseCase getAvailablePeriodsUseCase;
   @Autowired
   private GetProjectionUseCase getProjectionUseCase;

   @RequestMapping(value = API_PATH + "/{identification}/{period}", method = GET)
   public CommissionReport getCommissionsByPeriod(@PathVariable("identification") Long identification,
                                                  @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period)
         throws InternalException, ParseException {
      LOGGER.info("Retrieving commission for employee {} of the period {}", identification, period);
      String[] date = period.split("-");
      return getCommissionReportUseCase
            .getCommissionReport(identification, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
   }

   @RequestMapping(value = API_PATH + "/sub/{name}/{period}", method = GET)
   public List<CommissionReport> getSubDistributorCommissionsByPeriod(@PathVariable("name") String name,
                                                                      @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period) throws ParseException {
      LOGGER.info("Retrieving commissions for subdistributor {} of the period {}", name, period);
      String[] date = period.split("-");
      return getCommissionReportUseCase
            .getCommissionReport(name, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
   }

   @RequestMapping(value = API_PATH + "/{identification}", method = GET)
   public List<CommissionReport> getCommissions(@PathVariable("identification") Long identification)
         throws InternalException {
      LOGGER.info("Retrieving commissions for employee {} ", identification);
      return getCommissionReportUseCase.getCommissionsReport(identification);
   }

   @RequestMapping(value = API_PATH + "/period/{identification}", method = GET)
   public Set<String> getAvailablePeriods(@PathVariable("identification") Long identification)
         throws InternalException {
      LOGGER.info("Retrieving periods for employee {} ", identification);
      return getAvailablePeriodsUseCase.getAvailablePeriodsUseCase(identification);
   }

   @RequestMapping(value = API_PATH + "/sub/period/{name}", method = GET)
   public Set<String> getSubAvailablePeriods(@PathVariable("name") String name) throws InternalException {
      LOGGER.info("Retrieving periods for subdistributor {} ", name);
      return getAvailablePeriodsUseCase.getSubAvailablePeriodsUseCase(name);
   }

   @RequestMapping(value = API_PATH + "/projection/{identification}/{period}", method = GET)
   public Projection getProjectionByPeriod(@PathVariable("identification") Long identification,
                                           @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period)
         throws InternalException, ParseException {
      LOGGER.info("Retrieving commissions projection for employee {} on period {}", identification, period);
      String[] date = period.split("-");
      return getProjectionUseCase.getProjection(identification, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
   }

   @RequestMapping(value = API_PATH + "/download/{identification}/{period}", method = GET)
   public ResponseEntity<Resource> downloadWholesalerCommissionReport(@PathVariable("identification") Long identification,
                                                                      @PathVariable("period") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM") String period) throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, ParseException {
      LOGGER.info("Downloading wholesaler commission report of {} for period {}", identification, period);
      String[] date = period.split("-");
      File file = getCommissionReportUseCase.downloadCommissionReport(identification, Integer.parseInt(date[0]), Integer.parseInt(date[1]));
      InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
      HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=commissions.csv");
      return ResponseEntity.ok()
            .contentLength(file.length())
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(resource);
   }
}
