package org.belltronic.wholesalers.entrypoints.rest.user;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.user.DeleteUserUseCase;
import org.belltronic.wholesalers.domain.usecase.user.RestoreUserUseCase;
import org.belltronic.wholesalers.domain.usecase.user.createuser.CreateUserUseCase;
import org.belltronic.wholesalers.domain.usecase.user.getuser.GetUserUseCase;
import org.belltronic.wholesalers.domain.usecase.user.updateuser.UpdateUserUseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
public class UserEndpoint {

   public static final String API_PATH = "/v1/user";

   private static final Logger LOGGER = LoggerFactory.getLogger(UserEndpoint.class);

   @Autowired
   private CreateUserUseCase createUserUseCase;
   @Autowired
   private GetUserUseCase getUserUseCase;
   @Autowired
   private UpdateUserUseCase updateUserUseCase;
   @Autowired
   private DeleteUserUseCase deleteUserUseCase;
   @Autowired
   private RestoreUserUseCase restoreUserUseCase;

   @RequestMapping(value = API_PATH, method = POST)
   public ApplicationUser createUser(@RequestBody ApplicationUser applicationUserDTO) throws InternalException {
      LOGGER.info("Creating user with email: {}", applicationUserDTO.getEmail());
      return createUserUseCase.createUser(applicationUserDTO);
   }

   @RequestMapping(value = API_PATH + "/{email:.+}", method = GET)
   public ApplicationUser readUser(@PathVariable("email") String email) {
      LOGGER.info("Retrieving user for email: {}", email);
      return getUserUseCase.readUser(email);
   }

   @RequestMapping(value = API_PATH + "/{email:.+}", method = PUT)
   public ApplicationUser updateUser(@PathVariable("email") String email,
                                     @RequestBody ApplicationUser applicationUserDTO) throws InternalException {
      LOGGER.info("Updating user with email: {}", applicationUserDTO.getEmail());
      return updateUserUseCase.updateUser(email, applicationUserDTO);
   }

   @RequestMapping(value = API_PATH + "/{email:.+}", method = DELETE)
   public void deleteUser(@PathVariable("email") String email) throws InternalException {
      LOGGER.info("Deleting user for email: {}", email);
      deleteUserUseCase.deleteUser(email);
   }

   @RequestMapping(value = API_PATH + "/employee/{identification}", method = GET)
   public ApplicationUser readUserByEmployee(@PathVariable("identification") Long identification)
         throws InternalException {
      LOGGER.info("Retrieving user for employee: {}", identification);
      return getUserUseCase.readUserByEmployee(identification);
   }

   @RequestMapping(value = API_PATH + "/employee/{identification}", method = DELETE)
   public void deleteUserByEmployee(@PathVariable("identification") Long identification) throws InternalException {
      LOGGER.info("Deleting user for email: {}", identification);
      deleteUserUseCase.deleteUserByEmployee(identification);
   }

   @RequestMapping(value = API_PATH + "/restore/{email:.+}", method = GET)
   public ApplicationUser restoreUser(@PathVariable("email") String email) throws InternalException {
      LOGGER.info("Restoring password for user with email: {}", email);
      return restoreUserUseCase.restoreUser(email);
   }
}
