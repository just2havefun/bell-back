package org.belltronic.wholesalers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class WholesalerApplication {

   public static void main(String[] args) {
      SpringApplication.run(WholesalerApplication.class, args);
   }
}
