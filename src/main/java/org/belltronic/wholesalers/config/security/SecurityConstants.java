package org.belltronic.wholesalers.config.security;

public class SecurityConstants {

   public static final String SECRET = "Just2HaveFun";
   public static final String TOKEN_PREFIX = "j2hf ";
   public static final String HEADER_STRING = "Authorization";
   public static final long EXPIRATION_TIME = 864_000_000L;

}
