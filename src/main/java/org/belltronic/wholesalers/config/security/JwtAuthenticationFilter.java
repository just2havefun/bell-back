package org.belltronic.wholesalers.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.belltronic.wholesalers.providers.jpa.entity.ApplicationUserData;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.belltronic.wholesalers.config.security.SecurityConstants.EXPIRATION_TIME;
import static org.belltronic.wholesalers.config.security.SecurityConstants.HEADER_STRING;
import static org.belltronic.wholesalers.config.security.SecurityConstants.SECRET;
import static org.belltronic.wholesalers.config.security.SecurityConstants.TOKEN_PREFIX;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

   private AuthenticationManager authenticationManager;

   public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
      this.authenticationManager = authenticationManager;
   }

   @Override
   public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
      try {
         ApplicationUserData user = new ObjectMapper().readValue(request.getInputStream(), ApplicationUserData.class);
         return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

   @Override
   protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException {
      ZonedDateTime expirationTimeUTC = ZonedDateTime.now(ZoneOffset.UTC).plus(EXPIRATION_TIME, ChronoUnit.MILLIS);
      String token = Jwts.builder().setSubject(((User) authResult.getPrincipal()).getUsername())
            .setExpiration(Date.from(expirationTimeUTC.toInstant()))
            .signWith(SignatureAlgorithm.HS256, SECRET)
            .compact();
      JwtToken jwtToken = new JwtToken(TOKEN_PREFIX + token, Date.from(expirationTimeUTC.toInstant()));
      response.getWriter().append(new ObjectMapper().writeValueAsString(jwtToken));
      response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
   }
}
