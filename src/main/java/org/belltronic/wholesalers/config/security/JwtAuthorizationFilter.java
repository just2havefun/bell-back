package org.belltronic.wholesalers.config.security;

import io.jsonwebtoken.Jwts;
import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.usecase.security.LoadUserByUsernameUseCase;
import org.belltronic.wholesalers.domain.usecase.user.getuser.GetUserUseCase;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.belltronic.wholesalers.config.security.SecurityConstants.HEADER_STRING;
import static org.belltronic.wholesalers.config.security.SecurityConstants.SECRET;
import static org.belltronic.wholesalers.config.security.SecurityConstants.TOKEN_PREFIX;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

   private final GetUserUseCase getUserUseCase;
   private final LoadUserByUsernameUseCase loadUserByUsernameUseCase;

   public JwtAuthorizationFilter(AuthenticationManager authenticationManager, GetUserUseCase getUserUseCase,
         LoadUserByUsernameUseCase loadUserByUsernameUseCase) {
      super(authenticationManager);
      this.getUserUseCase = getUserUseCase;
      this.loadUserByUsernameUseCase = loadUserByUsernameUseCase;
   }

   @Override
   protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
         throws IOException, ServletException {
      String header = request.getHeader(HEADER_STRING);
      if (header == null || !header.startsWith(TOKEN_PREFIX)) {
         response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "unauthorized!");
      } else {
         try {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = getAuthenticationToken(request);
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            chain.doFilter(request, response);
         } catch (RuntimeException e) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "unauthorized!");
         }
      }
   }

   private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request) {
      String token = request.getHeader(HEADER_STRING);
      if (token == null)
         return null;
      String username = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody()
            .getSubject();

      try {
         ApplicationUser applicationUser = getUserUseCase.readUser(username);
         UserDetails userDetails = loadUserByUsernameUseCase.loadUserByUsername(username);
         return username != null ?
               new UsernamePasswordAuthenticationToken(applicationUser, null, userDetails.getAuthorities()) :
               null;
      } catch (InternalException e) {
         return null;
      }

   }
}
