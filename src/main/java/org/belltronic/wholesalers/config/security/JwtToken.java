package org.belltronic.wholesalers.config.security;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

class JwtToken {

   private String jwtToken;

   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
   private Date expirationTime;

   JwtToken(String jwtToken, Date expirationTime) {
      this.jwtToken = jwtToken;
      this.expirationTime = expirationTime;
   }

   public String getJwtToken() {
      return jwtToken;
   }

   public void setJwtToken(String jwtToken) {
      this.jwtToken = jwtToken;
   }

   public Date getExpirationTime() {
      return expirationTime;
   }

   public void setExpirationTime(Date expirationTime) {
      this.expirationTime = expirationTime;
   }
}
