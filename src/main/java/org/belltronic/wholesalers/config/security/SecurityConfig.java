package org.belltronic.wholesalers.config.security;

import org.belltronic.wholesalers.domain.usecase.security.LoadUserByUsernameUseCase;
import org.belltronic.wholesalers.domain.usecase.user.getuser.GetUserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

   @Autowired
   private GetUserUseCase getUserUseCase;
   @Autowired
   private LoadUserByUsernameUseCase loadUserByUsernameUseCase;

   @Value("${security.enabled:true}")
   private boolean securityEnabled;

   @Override
   public void configure(WebSecurity web) {
      if (securityEnabled) {
         web.ignoring().antMatchers("/v2/**", "/configuration/ui", "/swagger-resources/**", "/configuration/security",
               "/swagger-ui.html", "/webjars/**", "/location");
      } else {
         web.ignoring().antMatchers("/**");
      }
   }

   @Override
   protected void configure(HttpSecurity http) throws Exception {
      if (securityEnabled) {
         http.headers().frameOptions().sameOrigin();
         http.cors().and().csrf().disable().authorizeRequests().antMatchers("/v1/**").permitAll().and()
               .addFilter(new JwtAuthenticationFilter(authenticationManager())).addFilter(
               new JwtAuthorizationFilter(authenticationManager(), getUserUseCase, loadUserByUsernameUseCase));
      }
   }

   @Bean
   public PasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
   }


   @SuppressWarnings("deprecation")
   @Bean
   public WebMvcConfigurer corsConfigurer() {
      return new WebMvcConfigurerAdapter() {

         @Override
         public void addCorsMappings(CorsRegistry registry) {
            registry.addMapping("/login");
         }
      };
   }


}
