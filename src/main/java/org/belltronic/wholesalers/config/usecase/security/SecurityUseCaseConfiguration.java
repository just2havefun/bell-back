package org.belltronic.wholesalers.config.usecase.security;

import org.belltronic.wholesalers.domain.usecase.security.LoadUserByUsernameUseCase;
import org.belltronic.wholesalers.providers.jpa.LoadUserByUsernameProvider;
import org.belltronic.wholesalers.providers.jpa.repository.OldUserRepository;
import org.belltronic.wholesalers.providers.jpa.repository.SubDistributorRepository;
import org.belltronic.wholesalers.providers.jpa.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class SecurityUseCaseConfiguration {

   @Bean
   LoadUserByUsernameUseCase loadUserByUsernameUseCase(UserDetailsService loadUserByUsernameProvider) {
      return new LoadUserByUsernameUseCase(loadUserByUsernameProvider);
   }

   @Bean
   public LoadUserByUsernameProvider loadUserByUsernameProvider(UserRepository userRepository,
                                                                OldUserRepository oldUserRepository,
                                                                SubDistributorRepository subDistributorRepository) {
      return new LoadUserByUsernameProvider(userRepository, oldUserRepository, subDistributorRepository);
   }
}
