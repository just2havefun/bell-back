package org.belltronic.wholesalers.config.mail;

import org.belltronic.wholesalers.providers.mailproviders.spring.SendEmailProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailProviderConfiguration {

   @Bean
   SendEmailProvider sendEmailProvider() {
      return new SendEmailProvider();
   }
}
