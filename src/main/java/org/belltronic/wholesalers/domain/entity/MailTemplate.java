package org.belltronic.wholesalers.domain.entity;

public enum MailTemplate {

   NEW_USER("htmlTemplates/newUser.html");

   private String resourcePath;

   MailTemplate(String resourcePath) {
      this.resourcePath = resourcePath;
   }

   @Override
   public String toString() {
      return resourcePath;
   }
}
