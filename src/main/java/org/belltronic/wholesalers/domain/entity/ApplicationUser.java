package org.belltronic.wholesalers.domain.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.NotNull;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class ApplicationUser {

   private static final String PHP_PREFIX = "$2y";
   private static final String JAVA_PREFIX = "$2a";

   @NotNull
   private String email;

   private String password;

   private Employee employee;

   private String subdistributorName;

   private Type type;

   private Role role;

   public ApplicationUser() {
   }

   public ApplicationUser(String email, String password, String subdistributorName, boolean isAdmin) {
      this.email = email;
      this.password = password.replace(PHP_PREFIX, JAVA_PREFIX);
      this.subdistributorName = subdistributorName;
      if (isAdmin) {
         this.role = Role.ADMIN;
      } else {
         this.role = Role.GENERAL;
      }
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public Employee getEmployee() {
      return employee;
   }

   public void setEmployee(Employee employee) {
      this.employee = employee;
   }

   public Type getType() {
      return type;
   }

   public void setType(Type type) {
      this.type = type;
   }

   public Role getRole() {
      return role;
   }

   public void setRole(Role role) {
      this.role = role;
   }

   public String getSubdistributorName() {
      return subdistributorName;
   }

   public void setSubdistributorName(String subdistributorName) {
      this.subdistributorName = subdistributorName;
   }

   public boolean isAdmin() {
      return this.role == Role.ADMIN;
   }

   public enum Type {
      EMPLOYEE,
      SUBDISTRIBUTOR,
      DISTRIBUTOR
   }

   public enum Role {
      ADMIN,
      GENERAL
   }
}
