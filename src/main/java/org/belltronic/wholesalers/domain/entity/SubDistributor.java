package org.belltronic.wholesalers.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubDistributor {

   @NotNull
   private String name;
   @NotNull
   private String email;
   @NotNull
   @JsonIgnore
   private String password;
   @NotNull
   private Integer commission;

   @JsonInclude(JsonInclude.Include.NON_EMPTY)
   private List<Employee> advisers;
   @JsonInclude(JsonInclude.Include.NON_EMPTY)
   private List<Employee> wholesalers;

   public SubDistributor() {
   }

   public SubDistributor(@NotNull String name, @NotNull Integer commission) {
      this.name = name;
      this.commission = commission;
      this.advisers = new ArrayList<>();
      this.wholesalers = new ArrayList<>();
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Integer getCommission() {
      return commission;
   }

   public void setCommission(Integer commission) {
      this.commission = commission;
   }

   public List<Employee> getAdvisers() {
      return advisers;
   }

   public void setAdvisers(List<Employee> advisers) {
      this.advisers = advisers;
   }

   public List<Employee> getWholesalers() {
      return wholesalers;
   }

   public void setWholesalers(List<Employee> wholesalers) {
      this.wholesalers = wholesalers;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }
}
