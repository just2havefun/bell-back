package org.belltronic.wholesalers.domain.entity.report;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Builder
@Setter
public class RechargeReport {

   private Long employeeIdentification;
   private String employeeName;
   private Integer year;
   private Integer month;
   private BigDecimal prepaidRecharge;
   private BigDecimal freeRecharge;

}
