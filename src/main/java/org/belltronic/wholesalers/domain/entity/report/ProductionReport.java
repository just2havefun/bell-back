package org.belltronic.wholesalers.domain.entity.report;

import java.math.BigDecimal;

public class ProductionReport {

   private Long employeeIdentification;
   private String employeeName;
   private String period;
   private BigDecimal prepaidProduction;
   private BigDecimal freeProduction;

   public ProductionReport(Long employeeIdentification, String employeeName, String period, BigDecimal prepaidProduction, BigDecimal freeProduction) {
      this.employeeIdentification = employeeIdentification;
      this.employeeName = employeeName;
      this.prepaidProduction = prepaidProduction;
      this.freeProduction = freeProduction;
   }

   public Long getEmployeeIdentification() {
      return employeeIdentification;
   }

   public void setEmployeeIdentification(Long employeeIdentification) {
      this.employeeIdentification = employeeIdentification;
   }

   public String getEmployeeName() {
      return employeeName;
   }

   public void setEmployeeName(String employeeName) {
      this.employeeName = employeeName;
   }

   public String getPeriod() {
      return period;
   }

   public void setPeriod(String period) {
      this.period = period;
   }

   public BigDecimal getPrepaidProduction() {
      return prepaidProduction;
   }

   public void setPrepaidProduction(BigDecimal prepaidProduction) {
      this.prepaidProduction = prepaidProduction;
   }

   public BigDecimal getFreeProduction() {
      return freeProduction;
   }

   public void setFreeProduction(BigDecimal freeProduction) {
      this.freeProduction = freeProduction;
   }
}
