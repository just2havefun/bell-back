package org.belltronic.wholesalers.domain.entity.report.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class EmployeeReport {

   @CsvBindByPosition(position = 0)
   private String name;

   @CsvBindByPosition(position = 1)
   private String type;

   @CsvBindByPosition(position = 2)
   private Integer pack;

   @CsvBindByPosition(position = 3)
   private Integer packOrder;

   @CsvBindByPosition(position = 4)
   private String number;

   @CsvBindByPosition(position = 5)
   private String icc;

   @CsvBindByPosition(position = 6)
   @CsvDate("yyyy-MM-dd")
   private Date deliveryDate;

   @CsvBindByPosition(position = 7)
   @CsvDate("yyyy-MM-dd")
   private Date activationDate;

   @CsvBindByPosition(position = 8)
   @CsvDate("yyyy-MM-dd")
   private Date dueDate;

   @CsvBindByPosition(position = 9)
   private String state;

   @CsvBindByPosition(position = 10)
   private String codScl;

   @CsvBindByPosition(position = 11)
   private String codPunto;

   @CsvBindByPosition(position = 12)
   private String planCode;

   @CsvBindByPosition(position = 13)
   private Integer value;

   public EmployeeReport(String name, String type, Integer pack, Integer packOrder, String number, String icc, Date deliveryDate, Date activationDate, Date dueDate, String state, String codScl, String codPunto, String planCode, Integer value) {
      this.name = name;
      this.type = type;
      this.pack = pack;
      this.packOrder = packOrder;
      this.number = number;
      this.icc = '"' + icc + '"';
      this.deliveryDate = deliveryDate;
      this.activationDate = activationDate;
      this.dueDate = dueDate;
      this.state = state;
      this.codScl = codScl;
      this.codPunto = codPunto;
      this.planCode = planCode;
      this.value = value;
   }
}
