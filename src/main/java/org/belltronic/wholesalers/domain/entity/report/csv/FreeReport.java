package org.belltronic.wholesalers.domain.entity.report.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class FreeReport {

   @CsvBindByPosition(position = 0)
   private String distributorName;

   @CsvBindByPosition(position = 1)
   private String subDistributorName;

   @CsvBindByPosition(position = 2)
   private String employeeName;

   @CsvBindByPosition(position = 3)
   private Integer pack;

   @CsvBindByPosition(position = 4)
   private Integer packOrder;

   @CsvBindByPosition(position = 5)
   private String number;

   @CsvBindByPosition(position = 6)
   private String icc;

   @CsvBindByPosition(position = 7)
   @CsvDate("yyyy-MM-dd")
   private Date deliveryDate;

   @CsvBindByPosition(position = 8)
   @CsvDate("yyyy-MM-dd")
   private Date activationDate;

   @CsvBindByPosition(position = 9)
   @CsvDate("yyyy-MM-dd")
   private Date dueDate;

   @CsvBindByPosition(position = 10)
   private String state;

   @CsvBindByPosition(position = 11)
   private String enterpriseName;

   @CsvBindByPosition(position = 12)
   private String NIT;

   @CsvBindByPosition(position = 13)
   private String enterpriseAddress;

   @CsvBindByPosition(position = 14)
   private String codScl;

   @CsvBindByPosition(position = 15)
   private String codPunto;

   @CsvBindByPosition(position = 16)
   private String planCode;

   @CsvBindByPosition(position = 17)
   private Integer value;

   @CsvBindByPosition(position = 18)
   private String responsible;

   @CsvBindByPosition(position = 19)
   private String responsibleIdentification;

   @CsvBindByPosition(position = 20)
   private String responsibleAddress;

   @CsvBindByPosition(position = 21)
   private String responsibleCity;

   @CsvBindByPosition(position = 22)
   private String responsibleNeighborhood;

   @CsvBindByPosition(position = 23)
   private String responsiblePhone;

   @CsvBindByPosition(position = 24)
   private String callDetail;

   @CsvBindByPosition(position = 25)
   @CsvDate("yyyy-MM-dd")
   private Date callDate;

}
