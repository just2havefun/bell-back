package org.belltronic.wholesalers.domain.entity;

import java.math.BigDecimal;

public class Projection {

   private Long employeeIdentification;
   private String employeeName;
   private Integer year;
   private Integer month;
   private BigDecimal prepaidCommission;
   private BigDecimal freeCommission;

   public Projection(Long employeeIdentification, String employeeName, Integer year, Integer month) {
      this.employeeIdentification = employeeIdentification;
      this.employeeName = employeeName;
      this.year = year;
      this.month = month;
      this.prepaidCommission = BigDecimal.ZERO;
      this.freeCommission = BigDecimal.ZERO;
   }

   public Long getEmployeeIdentification() {
      return employeeIdentification;
   }

   public void setEmployeeIdentification(Long employeeIdentification) {
      this.employeeIdentification = employeeIdentification;
   }

   public BigDecimal getPrepaidCommission() {
      return prepaidCommission;
   }

   public void setPrepaidCommission(BigDecimal prepaidCommission) {
      this.prepaidCommission = prepaidCommission;
   }

   public BigDecimal getFreeCommission() {
      return freeCommission;
   }

   public void setFreeCommission(BigDecimal freeCommission) {
      this.freeCommission = freeCommission;
   }

   public Integer getYear() {
      return year;
   }

   public void setYear(Integer year) {
      this.year = year;
   }

   public Integer getMonth() {
      return month;
   }

   public void setMonth(Integer month) {
      this.month = month;
   }

   public String getEmployeeName() {
      return employeeName;
   }

   public void setEmployeeName(String employeeName) {
      this.employeeName = employeeName;
   }
}
