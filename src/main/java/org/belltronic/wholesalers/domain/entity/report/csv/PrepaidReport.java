package org.belltronic.wholesalers.domain.entity.report.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class PrepaidReport {

   @CsvBindByPosition(position = 0)
   private String distributorName;

   @CsvBindByPosition(position = 1)
   private String subDistributorName;

   @CsvBindByPosition(position = 2)
   private String employeeName;

   @CsvBindByPosition(position = 3)
   private Integer pack;

   @CsvBindByPosition(position = 4)
   private Integer packOrder;

   @CsvBindByPosition(position = 5)
   private String number;

   @CsvBindByPosition(position = 6)
   private String icc;

   @CsvBindByPosition(position = 7)
   @CsvDate("yyyy-MM-dd")
   private Date deliveryDate;

   @CsvBindByPosition(position = 8)
   @CsvDate("yyyy-MM-dd")
   private Date activationDate;

   @CsvBindByPosition(position = 9)
   @CsvDate("yyyy-MM-dd")
   private Date dueDate;

   @CsvBindByPosition(position = 10)
   private String state;

}
