package org.belltronic.wholesalers.domain.entity.report.csv;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class SimcardReport {

   private String distributorName;

   private String subDistributorName;

   private String employeeName;

   private Integer pack;

   private Integer packOrder;

   private String number;

   private String icc;

   private Date deliveryDate;

   private Date activationDate;

   private Date dueDate;

   private String state;

   private String enterpriseName;

   private String NIT;

   private String enterpriseAddress;

   private String codScl;

   private String codPunto;

   private String planCode;

   private Integer value;

   private String responsible;

   private String responsibleIdentification;

   private String responsibleAddress;

   private String responsibleCity;

   private String responsibleNeighborhood;

   private String responsiblePhone;

   private String callDetail;

   private Date callDate;

   public SimcardReport(String distributorName, String subDistributorName, String employeeName, Integer pack, Integer packOrder, String number, String icc, Date deliveryDate, Date activationDate, Date dueDate, String state, String enterpriseName, String NIT, String enterpriseAddress, String codScl, String codPunto, String planCode, Integer value, String responsible, String responsibleIdentification, String responsibleAddress, String responsibleCity, String responsibleNeighborhood, String responsiblePhone, String callDetail, Date callDate) {
      this.distributorName = distributorName;
      this.subDistributorName = subDistributorName;
      this.employeeName = employeeName;
      this.pack = pack;
      this.packOrder = packOrder;
      this.number = number;
      this.icc = '"' + icc + '"';
      this.deliveryDate = deliveryDate;
      this.activationDate = activationDate;
      this.dueDate = dueDate;
      this.state = state;
      this.enterpriseName = enterpriseName;
      this.NIT = NIT;
      this.enterpriseAddress = enterpriseAddress;
      this.codScl = codScl;
      this.codPunto = codPunto;
      this.planCode = planCode;
      this.value = value;
      this.responsible = responsible;
      this.responsibleIdentification = responsibleIdentification;
      this.responsibleAddress = responsibleAddress;
      this.responsibleCity = responsibleCity;
      this.responsibleNeighborhood = responsibleNeighborhood;
      this.responsiblePhone = responsiblePhone;
      this.callDetail = callDetail;
      this.callDate = callDate;
   }
}
