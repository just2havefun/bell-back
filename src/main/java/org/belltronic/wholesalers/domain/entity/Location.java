package org.belltronic.wholesalers.domain.entity;

import javax.validation.constraints.NotNull;

public class Location {

   @NotNull
   private Long employeeIdentification;
   @NotNull
   private Double latitude;
   @NotNull
   private Double longitude;

   public Long getEmployeeIdentification() {
      return employeeIdentification;
   }

   public void setEmployeeIdentification(Long employeeIdentification) {
      this.employeeIdentification = employeeIdentification;
   }

   public Double getLatitude() {
      return latitude;
   }

   public void setLatitude(Double latitude) {
      this.latitude = latitude;
   }

   public Double getLongitude() {
      return longitude;
   }

   public void setLongitude(Double longitude) {
      this.longitude = longitude;
   }
}
