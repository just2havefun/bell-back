package org.belltronic.wholesalers.domain.entity.report.csv;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;
import java.util.Date;

@Setter
@Getter
public class DetailCommission {

   @CsvBindByPosition(position = 0)
   private String employeeName;
   @CsvBindByPosition(position = 1)
   private String number;
   @CsvBindByPosition(position = 2)
   private String icc;
   @CsvBindByPosition(position = 3)
   private String type;
   @CsvBindByPosition(position = 4)
   @CsvDate("yyyy-MM-dd")
   private Date deliveryDate;
   @CsvBindByPosition(position = 5)
   @CsvDate("yyyy-MM-dd")
   private Date activationDate;
   @CsvBindByPosition(position = 6)
   @CsvDate("yyyy-MM-dd")
   private Date dueDate;
   @CsvBindByPosition(position = 7)
   private String state;
   @CsvBindByPosition(position = 8)
   private BigInteger recharge;
   @CsvBindByPosition(position = 9)
   private BigInteger neto;
   @CsvBindByPosition(position = 10)
   private BigInteger comission;

   public DetailCommission(String employeeName, String number, String icc, int type, Date deliveryDate, Date activationDate, Date dueDate, BigInteger recharge, String state) {
      this.employeeName = employeeName;
      this.number = number;
      this.icc = '"' + icc + '"';
      this.type = type == 1 ? "PREPAGO" : "LIBRE";
      this.deliveryDate = deliveryDate;
      this.activationDate = activationDate;
      this.dueDate = dueDate;
      this.recharge = recharge;
      this.state = state;
   }

}
