package org.belltronic.wholesalers.domain.entity.report;

import java.math.BigDecimal;

public class CommissionReport {

   private Long employeeIdentification;
   private String employeeName;
   private Integer year;
   private Integer month;
   private BigDecimal prepaidCommission;
   private BigDecimal prepaidIca;
   private BigDecimal prepaidRete;
   private BigDecimal freeCommission;
   private BigDecimal freeIca;
   private BigDecimal freeRete;

   public CommissionReport(Long employeeIdentification, String employeeName, Integer year, Integer month) {
      this.employeeIdentification = employeeIdentification;
      this.employeeName = employeeName;
      this.year = year;
      this.month = month;
   }

   public Long getEmployeeIdentification() {
      return employeeIdentification;
   }

   public void setEmployeeIdentification(Long employeeIdentification) {
      this.employeeIdentification = employeeIdentification;
   }

   public BigDecimal getPrepaidCommission() {
      return prepaidCommission;
   }

   public void setPrepaidCommission(BigDecimal prepaidCommission) {
      this.prepaidCommission = prepaidCommission;
   }

   public BigDecimal getFreeCommission() {
      return freeCommission;
   }

   public void setFreeCommission(BigDecimal freeCommission) {
      this.freeCommission = freeCommission;
   }

   public Integer getYear() {
      return year;
   }

   public void setYear(Integer year) {
      this.year = year;
   }

   public Integer getMonth() {
      return month;
   }

   public void setMonth(Integer month) {
      this.month = month;
   }

   public String getEmployeeName() {
      return employeeName;
   }

   public void setEmployeeName(String employeeName) {
      this.employeeName = employeeName;
   }

   public BigDecimal getPrepaidIca() {
      return prepaidIca;
   }

   public void setPrepaidIca(BigDecimal prepaidIca) {
      this.prepaidIca = prepaidIca;
   }

   public BigDecimal getPrepaidRete() {
      return prepaidRete;
   }

   public void setPrepaidRete(BigDecimal prepaidRete) {
      this.prepaidRete = prepaidRete;
   }

   public BigDecimal getFreeIca() {
      return freeIca;
   }

   public void setFreeIca(BigDecimal freeIca) {
      this.freeIca = freeIca;
   }

   public BigDecimal getFreeRete() {
      return freeRete;
   }

   public void setFreeRete(BigDecimal freeRete) {
      this.freeRete = freeRete;
   }
}
