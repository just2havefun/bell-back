package org.belltronic.wholesalers.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Distributor {

   @NotNull
   private String name;
   @NotNull
   private String email;

   @JsonInclude(JsonInclude.Include.NON_EMPTY)
   private List<SubDistributor> subDistributors;

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public List<SubDistributor> getSubDistributors() {
      return subDistributors;
   }

   public void setSubDistributors(List<SubDistributor> subDistributors) {
      this.subDistributors = subDistributors;
   }
}
