package org.belltronic.wholesalers.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Advisor {

   private Long identification;

   private String name;

}
