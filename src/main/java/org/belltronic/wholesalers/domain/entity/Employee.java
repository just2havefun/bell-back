package org.belltronic.wholesalers.domain.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class Employee {

   @NotNull
   private Long identification;
   @NotNull
   private String name;
   private Boolean locatable = false;
   private Long phone;
   private String address;
   @NotNull
   private Integer commission;
   @NotNull
   private String leaderName;
   @NotNull
   private Role role;

   private ApplicationUser user;

   public Long getIdentification() {
      return identification;
   }

   public void setIdentification(Long identification) {
      this.identification = identification;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Boolean getLocatable() {
      return locatable;
   }

   public void setLocatable(Boolean locatable) {
      this.locatable = locatable;
   }

   public Long getPhone() {
      return phone;
   }

   public void setPhone(Long phone) {
      this.phone = phone;
   }

   public String getAddress() {
      return address;
   }

   public void setAddress(String address) {
      this.address = address;
   }

   public Integer getCommission() {
      return commission;
   }

   public void setCommission(Integer commission) {
      this.commission = commission;
   }

   public String getLeaderName() {
      return leaderName;
   }

   public void setLeaderName(String leaderName) {
      this.leaderName = leaderName;
   }

   public Role getRole() {
      return role;
   }

   public void setRole(Role role) {
      this.role = role;
   }

   public enum Role {
      WHOLESALER,
      ADVISER
   }

   public ApplicationUser getUser() {
      return user;
   }

   public void setUser(ApplicationUser user) {
      this.user = user;
   }
}
