package org.belltronic.wholesalers.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MailMessage implements Serializable {

   private String to;

   private String subject;

   private String content;

   private String contentType;

   @Builder.Default
   private List<File> attachments = new ArrayList<>();

}
