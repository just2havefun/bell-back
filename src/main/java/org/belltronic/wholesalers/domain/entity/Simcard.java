package org.belltronic.wholesalers.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Simcard {

   private String number;
   private String icc;
   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   private Date dueDate;
   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   private Date deliveryDate;
   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   private Date activationDate;
   private Integer type;
   private Integer pack;
   private Long employeeIdentification;
   private String employeeName;
   private String subDistributorName;
   private String plan;
   private Integer value;
   private Integer packOrder;

}
