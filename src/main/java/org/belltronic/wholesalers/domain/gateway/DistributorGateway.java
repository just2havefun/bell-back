package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Distributor;

import java.util.List;

public interface DistributorGateway {

   Distributor readDistributorByEmail(String email);

   List<Distributor> listDistributors();

}
