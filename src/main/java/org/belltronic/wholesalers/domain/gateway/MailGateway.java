package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.springframework.scheduling.annotation.Async;

public interface MailGateway {

   @Async
   void sendEmail(MailMessage mailMessage);
}
