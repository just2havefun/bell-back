package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Simcard;

import java.util.List;

public interface SimcardGateway {

   Simcard readSimcard(String id);

   Simcard updateSimcard(Simcard simcard);

   Simcard packSimcard(Simcard simcard);

   List<Simcard> listSimcardsByTypeAndOwner(Integer type, String ownerName);

   List<Simcard> getPack(final int packNumber);

   int getLastPack();

   void assignPackToEmployee(final int packNumber, final long employeeIdentification);

   void unassignPack(final int packNumber);

   void updateNumberByIcc(final String number, final String icc);

   void deleteConflictedNumberByIcc(final String number, final String icc);

}
