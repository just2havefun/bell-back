package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;

public interface UserGateway {

   ApplicationUser createUser(ApplicationUser applicationUser);

   ApplicationUser readUser(String email);

   ApplicationUser updateUser(String email, ApplicationUser applicationUser);

   void deleteUser(String email);

   void deleteUserByEmployee(Long identification);

   ApplicationUser readUserByEmployee(Long identification);
}
