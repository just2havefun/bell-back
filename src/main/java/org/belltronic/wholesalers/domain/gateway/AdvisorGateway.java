package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Advisor;

public interface AdvisorGateway {

   Advisor readAdvisor(Long identification);

}
