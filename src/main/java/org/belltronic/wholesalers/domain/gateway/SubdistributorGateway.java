package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.SubDistributor;

import java.util.List;

public interface SubdistributorGateway {

   SubDistributor readSubdistributor(String name);

   SubDistributor readSubdistributorByEmail(String email);

   List<SubDistributor> listSubDistributors();

   List<SubDistributor> listSubDistributorsByLeader(String leaderEmail);

   SubDistributor updateSubdistributor(SubDistributor subDistributor);

}
