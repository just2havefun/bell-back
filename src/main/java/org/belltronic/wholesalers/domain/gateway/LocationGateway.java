package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Location;

public interface LocationGateway {

   Location createLocation(Location location);

}
