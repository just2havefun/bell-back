package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.report.ProductionReport;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface CommissionGateway {

   Set<String> getAvailablePeriods(Long employeeIdentification);

   Set<String> getSubAvailablePeriods(String name);

   List<ProductionReport> getSubProductionReport(String subDistributorName, String period, Date dueDate);
}
