package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Employee;

import java.util.List;

public interface EmployeeGateway {

   Employee createEmployee(Employee employeeDTO);

   Employee readEmployee(Long identification);

   Employee updateEmployee(Long identification, Employee employeeDTO);

   void deleteEmployee(Long identification);

   List<Employee> listEmployees();

   List<Employee> listEmployeesByRole(Employee.Role role);

   List<Employee> listEmployeesByRoleAndLeader(Employee.Role role, String leaderName);

}
