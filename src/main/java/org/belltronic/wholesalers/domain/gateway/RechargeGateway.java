package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.Projection;
import org.belltronic.wholesalers.domain.entity.report.CommissionReport;
import org.belltronic.wholesalers.domain.entity.report.RechargeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.DetailCommission;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

public interface RechargeGateway {

   Set<String> getAvailablePeriods(Long employeeIdentification);

   CommissionReport getCommissionReport(Long employeeIdentification, Integer year, Integer month) throws ParseException;

   List<CommissionReport> getCommissionsReport(Long employeeIdentification);

   List<CommissionReport> getSubCommissionsReport(String subDistributorName, Integer year, Integer month) throws ParseException;

   List<DetailCommission> getCommissionDetail(Long employeeIdentification, Integer year, Integer month) throws ParseException;

   Projection getProjection(Long identification, Integer commission, Integer year, Integer month) throws ParseException;

   List<RechargeReport> getSubRechargeReport(String subDistributorName, Integer year, Integer month) throws ParseException;

   void cleanRecharges();

}
