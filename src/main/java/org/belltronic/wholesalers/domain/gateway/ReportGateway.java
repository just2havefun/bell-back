package org.belltronic.wholesalers.domain.gateway;

import org.belltronic.wholesalers.domain.entity.CommissionHistory;
import org.belltronic.wholesalers.domain.entity.report.csv.EmployeeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.FreeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport;

import java.util.Date;
import java.util.List;

public interface ReportGateway {

   List<SimcardReport> listSimcardsByTypeAndOwnerAndDate(final int type,final String ownerName, final Date initialDate, final Date finalDate);

   List<SimcardReport> listSimcardsByTypeAndDate(final int type, final Date initialDate, final Date finalDate);

   List<EmployeeReport> listEmployeeSimcards(final Long identification);

   CommissionHistory getCommissionHistoryByPeriod(final Long employeeIdentification, final Integer year,
                                                  final Integer month);

   CommissionHistory createCommissionHistory(final CommissionHistory commissionHistory);

   void deleteCommissionHistory(final int id);

   List<CommissionHistory> listCommissionHistoryByEmployee(final Long employeeIdentification);
}
