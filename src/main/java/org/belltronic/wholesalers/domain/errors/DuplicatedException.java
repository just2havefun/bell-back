package org.belltronic.wholesalers.domain.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicatedException extends RuntimeException {

   private static final String MESSAGE = "Entidad duplicada";

   public DuplicatedException() {
      super(MESSAGE);
   }
}
