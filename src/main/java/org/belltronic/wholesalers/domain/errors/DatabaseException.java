package org.belltronic.wholesalers.domain.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DatabaseException extends RuntimeException {

   private static final String MESSAGE = "Error en la base de datos";

   public DatabaseException() {
      super(MESSAGE);
   }

   public DatabaseException(String msg, Throwable e) {
      super(msg, e);
   }
}
