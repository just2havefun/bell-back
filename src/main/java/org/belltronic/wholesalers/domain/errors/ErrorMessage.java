package org.belltronic.wholesalers.domain.errors;

public enum ErrorMessage {

   USER_NOT_FOUND("Usuario no encontrado"),
   USER_DUPLICATED("El Usuario ya existe"),
   USER_BY_EMPLOYEE_NOT_FOUND("El Empleado no tiene usuario asignado"),
   EMPLOYEE_ALREADY_HAS_AN_USER("El Empleado ya tiene usuario asignado"),
   WRONG_CREDENTIALS("Credenciales incorrectas"),
   EMPLOYEE_NOT_FOUND("Empleado no encontrado"),
   EMPLOYEE_DUPLICATED("El Empleado ya existe"),
   DISTRIBUTOR_NOT_FOUND("Distribuidor no encontrado"),
   ADVISER_NOT_FOUND("Asesor no encontrado"),
   SIMCARD_NOT_FOUND("SimcardData no encontrada"),
   INTERNAL_ERROR("Error Interno, por favor intente nuevamente. De persistir, contactese con el administrador");

   private String message;

   ErrorMessage(String message) {
      this.message = message;
   }

   @Override
   public String toString() {
      return this.message;
   }
}
