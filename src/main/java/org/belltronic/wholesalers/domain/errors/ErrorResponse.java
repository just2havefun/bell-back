package org.belltronic.wholesalers.domain.errors;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {
   private List<ErrorItem> errors;

   public ErrorResponse() {
      this.errors = new ArrayList<>();
   }

   public List<ErrorItem> getErrors() {
      return errors;
   }

   public void setErrors(List<ErrorItem> errors) {
      this.errors = errors;
   }

   public static class ErrorItem<T> {

      private T object;
      private String message;

      public T getObject() {
         return object;
      }

      public void setObject(T object) {
         this.object = object;
      }

      public String getMessage() {
         return message;
      }

      public void setMessage(String message) {
         this.message = message;
      }
   }
}
