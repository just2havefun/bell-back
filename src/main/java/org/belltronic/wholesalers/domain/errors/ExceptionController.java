package org.belltronic.wholesalers.domain.errors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ControllerAdvice
public class ExceptionController {

   private static final String INVALID_ARG_MESSAGE = "Argumento no valido";
   private final Pattern pattern = Pattern.compile("\"(.+?)\"");

   @ExceptionHandler(InternalException.class)
   public ResponseEntity handleInternalException(InternalException ex) {
      ErrorResponse errorResponse = new ErrorResponse();
      errorResponse.getErrors().add(ex.getErrorItem());
      return new ResponseEntity<>(errorResponse, ex.getHttpStatus());
   }

   @ExceptionHandler(TransactionSystemException.class)
   public ResponseEntity handleTxException(TransactionSystemException ex) {
      Throwable t = ExceptionUtils.getRootCause(ex);
      if (t instanceof ConstraintViolationException) {
         return handleConstraintException((ConstraintViolationException) t);
      } else {
         return new ResponseEntity<>(t.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
      }
   }

   @ExceptionHandler(IllegalArgumentException.class)
   public ResponseEntity handleIllegalIfException(IllegalArgumentException ex) {
      ErrorResponse errorResponse = new ErrorResponse();
      ErrorResponse.ErrorItem<String> errorItem = new ErrorResponse.ErrorItem<>();
      final Matcher matcher = pattern.matcher(ex.getLocalizedMessage());
      errorItem.setObject(matcher.find() ? matcher.group(1) : INVALID_ARG_MESSAGE);
      errorItem.setMessage(INVALID_ARG_MESSAGE);
      errorResponse.getErrors().add(errorItem);
      return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
   }

   @ExceptionHandler(ConstraintViolationException.class)
   public ResponseEntity handleConstraintException(ConstraintViolationException ex) {
      ErrorResponse errorResponse = new ErrorResponse();
      ErrorResponse.ErrorItem<String> errorItem;
      for (ConstraintViolation c : ex.getConstraintViolations()) {
         errorItem = new ErrorResponse.ErrorItem<>();
         errorItem.setObject(c.getPropertyPath().toString());
         errorItem.setMessage(c.getMessage());
         errorResponse.getErrors().add(errorItem);
      }
      return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
   }

}
