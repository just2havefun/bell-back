package org.belltronic.wholesalers.domain.errors;

import org.springframework.http.HttpStatus;

public class InternalException extends RuntimeException {

   private ErrorResponse.ErrorItem errorItem;
   private HttpStatus httpStatus;

   public InternalException(ErrorResponse.ErrorItem errorItem, HttpStatus httpStatus) {
      this.errorItem = errorItem;
      this.httpStatus = httpStatus;
   }

   public ErrorResponse.ErrorItem getErrorItem() {
      return errorItem;
   }

   public void setErrorItem(ErrorResponse.ErrorItem errorItem) {
      this.errorItem = errorItem;
   }

   public HttpStatus getHttpStatus() {
      return httpStatus;
   }

   public void setHttpStatus(HttpStatus httpStatus) {
      this.httpStatus = httpStatus;
   }
}
