package org.belltronic.wholesalers.domain.usecase.user.updateuser;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;

import java.util.Optional;

public interface UpdateUser {

   Optional<ApplicationUser> updateUser(final String email, final ApplicationUser applicationUser);
}
