package org.belltronic.wholesalers.domain.usecase.file.updateNumbersUseCase;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateNumberStatus {

   @CsvBindByPosition(position = 0)
   private String icc;
   @CsvBindByPosition(position = 1)
   private String number;
   @CsvBindByPosition(position = 2)
   private String status;
}
