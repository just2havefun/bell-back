package org.belltronic.wholesalers.domain.usecase.user.createuser;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.belltronic.wholesalers.domain.usecase.user.SendMailUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateUserUseCase {

   @Autowired
   private UserGateway userGateway;
   @Autowired
   private SendMailUseCase sendUserMailUseCase;

   public ApplicationUser createUser(final ApplicationUser applicationUserDTO) {
      ApplicationUser applicationUser = userGateway.createUser(applicationUserDTO);

      MailMessage mailMessage = MailMessage.builder()
            .to(applicationUser.getEmail())
            .subject("Creación de usuario Belltronic ERP")
            .content("<h1>Asignaci&oacute;n Usuario Belltronic</h1><h2>Email: " + applicationUser.getEmail()
                  + "</h2><h2>Password: " + applicationUser.getPassword() + "</h2>")
            .contentType("text/html")
            .build();

      sendUserMailUseCase.sendUserMail(applicationUser, mailMessage);

      return applicationUser;
   }
}
