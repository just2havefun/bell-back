package org.belltronic.wholesalers.domain.usecase.user;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.entity.SubDistributor;
import org.belltronic.wholesalers.domain.errors.InternalException;
import org.belltronic.wholesalers.domain.gateway.MailGateway;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SendMailUseCase {

   private static final Logger LOGGER = LoggerFactory.getLogger(SendMailUseCase.class);

   @Autowired
   private MailGateway sendEmail;
   @Autowired
   private UserGateway userGateway;

   @Async
   public void sendUserMail(ApplicationUser applicationUser, MailMessage mailMessage) {
      try {
         sendEmail.sendEmail(mailMessage);
      } catch (InternalException e) {
         try {
            userGateway.deleteUser(applicationUser.getEmail());
         } catch (InternalException e1) {
            LOGGER.error("Error compensating wrong applicationUser: " + applicationUser.getEmail());
         }
      }
   }

   @Async
   public void sendSubDsitributorMail(SubDistributor subDistributor, MailMessage mailMessage) {
      try {
         sendEmail.sendEmail(mailMessage);
      } catch (InternalException e) {
         LOGGER.error("Error sending mail to: " + subDistributor.getEmail());
      }
   }
}
