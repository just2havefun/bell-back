package org.belltronic.wholesalers.domain.usecase.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public class LoadUserByUsernameUseCase {

   private UserDetailsService userDetailsService;

   public LoadUserByUsernameUseCase(UserDetailsService userDetailsService) {
      this.userDetailsService = userDetailsService;
   }

   public UserDetails loadUserByUsername(final String username) {
      return userDetailsService.loadUserByUsername(username);
   }
}
