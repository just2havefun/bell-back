package org.belltronic.wholesalers.domain.usecase.report.production;

import org.belltronic.wholesalers.domain.entity.report.ProductionReport;
import org.belltronic.wholesalers.domain.gateway.CommissionGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class GetProductionReportUseCase {

   @Autowired
   private CommissionGateway commissionGateway;
   private SimpleDateFormat parser = new SimpleDateFormat("yyyy/MM/dd");

   public List<ProductionReport> getProductionReport(final String subDistributorName, final String period) throws ParseException {
      String year = period.substring(0, 3);
      String month = period.substring(4, 5);
      Date dueDate = parser.parse(year + "/" + month + "/1");
      return commissionGateway.getSubProductionReport(subDistributorName, period, dueDate);
   }

   public Set<String> getProductionAvailablePeriodsUseCase(final String name) {
      return commissionGateway.getSubAvailablePeriods(name);
   }
}
