package org.belltronic.wholesalers.domain.usecase.report.simcard;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.entity.report.csv.EmployeeReport;
import org.belltronic.wholesalers.domain.gateway.EmployeeGateway;
import org.belltronic.wholesalers.domain.gateway.ReportGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Service
public class GetEmployeeReportUseCase {

   private static String[] headerRecord = {"NOMBRE", "TIPO", "PAQUETE", "ORDEN", "NUMERO", "ICC", "FECHA ENTREGA", "FECHA ACTIVACION", "FECHA VENCIMIENTO", "ESTADO", "COD_SCL", "COD_PUNTO",
         "CODIGO PLAN", "VALOR PLAN"};

   private ReportGateway reportGateway;
   private EmployeeGateway employeeGateway;

   @Autowired
   public GetEmployeeReportUseCase(ReportGateway reportGateway, EmployeeGateway employeeGateway) {
      this.reportGateway = reportGateway;
      this.employeeGateway = employeeGateway;
   }

   public File getEmployeeReport(Long identification) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
      Employee employee = employeeGateway.readEmployee(identification);
      List<EmployeeReport> simcards = reportGateway.listEmployeeSimcards(identification);
      simcards.forEach((simcard) -> simcard.setName(employee.getName()));
      File file = new File("simcards.csv");
      Writer writer = new FileWriter("simcards.csv");
      StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
      CSVWriter csvWriter = new CSVWriter(writer,
            CSVWriter.DEFAULT_SEPARATOR,
            CSVWriter.NO_QUOTE_CHARACTER,
            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
            CSVWriter.DEFAULT_LINE_END);
      csvWriter.writeNext(headerRecord);
      beanToCsv.write(simcards);
      writer.close();
      return file;
   }
}
