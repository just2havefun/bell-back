package org.belltronic.wholesalers.domain.usecase.report.commission;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.entity.Projection;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.belltronic.wholesalers.domain.usecase.employee.CrudEmployeeUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class GetProjectionUseCase {

   @Autowired
   private RechargeGateway rechargeGateway;
   @Autowired
   private CrudEmployeeUseCase employeeUseCase;

   public Projection getProjection(final Long employeeIdentification, final Integer year,
                                   final Integer month) throws ParseException {

      Employee employeeDTO = employeeUseCase.readEmployee(employeeIdentification);

      return rechargeGateway.getProjection(employeeIdentification, employeeDTO.getCommission(), year, month);
   }
}
