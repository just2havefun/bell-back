package org.belltronic.wholesalers.domain.usecase.user;

import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteUserUseCase {

   @Autowired
   private UserGateway userGateway;

   public void deleteUser(final String email) {
      userGateway.deleteUser(email);
   }

   public void deleteUserByEmployee(final Long identification) {
      userGateway.deleteUserByEmployee(identification);
   }
}
