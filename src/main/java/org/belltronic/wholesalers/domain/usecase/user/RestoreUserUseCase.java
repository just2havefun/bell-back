package org.belltronic.wholesalers.domain.usecase.user;

import org.apache.commons.lang.RandomStringUtils;
import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestoreUserUseCase {

   @Autowired
   private UserGateway userGateway;
   @Autowired
   private SendMailUseCase sendUserMailUseCase;

   public ApplicationUser restoreUser(final String email) {
      ApplicationUser applicationUserDTO = userGateway.readUser(email);
      applicationUserDTO.setPassword(RandomStringUtils.random(10, true, true));
      ApplicationUser storedUser = userGateway.updateUser(email, applicationUserDTO);

      MailMessage mailMessage = MailMessage.builder()
            .to(storedUser.getEmail())
            .subject("Actualización de contraseña Belltronic ERP")
            .content("<h1>Se ha actualizado su contraseña</h1><h2>Email: " + storedUser.getEmail() + "</h2><h2>Password: "
                  + applicationUserDTO.getPassword() + "</h2>")
            .contentType("text/html")
            .build();

      sendUserMailUseCase.sendUserMail(applicationUserDTO, mailMessage);

      return storedUser;
   }
}
