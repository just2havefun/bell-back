package org.belltronic.wholesalers.domain.usecase.employee;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.gateway.EmployeeGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListEmployeesUseCase {

   @Autowired
   private EmployeeGateway employeeGateway;

   public List<Employee> listEmployees() {
      return employeeGateway.listEmployees();
   }

   public List<Employee> listAdvisers() {
      return employeeGateway.listEmployeesByRole(Employee.Role.ADVISER);
   }

   public List<Employee> listWholesalers() {
      return employeeGateway.listEmployeesByRole(Employee.Role.WHOLESALER);
   }

   public List<Employee> listAdvisersByLeader(String leaderName) {
      return employeeGateway.listEmployeesByRoleAndLeader(Employee.Role.ADVISER, leaderName);
   }

   public List<Employee> listWholesalersByLeader(String leaderName) {

      return employeeGateway.listEmployeesByRoleAndLeader(Employee.Role.WHOLESALER, leaderName);
   }

}
