package org.belltronic.wholesalers.domain.usecase.user.getuser;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetUserUseCase {

   @Autowired
   private UserGateway userGateway;

   public ApplicationUser readUser(final String email) {
      return userGateway.readUser(email);
   }

   public ApplicationUser readUserByEmployee(final Long identification) {
      return userGateway.readUserByEmployee(identification);
   }

}
