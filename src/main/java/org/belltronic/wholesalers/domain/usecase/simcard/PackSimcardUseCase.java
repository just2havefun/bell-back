package org.belltronic.wholesalers.domain.usecase.simcard;

import org.belltronic.wholesalers.domain.entity.Simcard;
import org.belltronic.wholesalers.domain.gateway.SimcardGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class PackSimcardUseCase {

   @Autowired
   private SimcardGateway simcardGateway;

   @Transactional
   public int packSimcards(List<Simcard> simcards) {
      int packNumer = simcardGateway.getLastPack() + 1;
      simcards.forEach(simcard -> {
         simcard.setPack(packNumer);
         simcardGateway.packSimcard(simcard);
      });
      return packNumer;
   }

   public List<Simcard> getPack(int packNumber) {
      return simcardGateway.getPack(packNumber);
   }

   @Transactional
   public List<Simcard> updatePack(int packNumber, List<Simcard> simcards) {
      List<Simcard> updatedSimcards = new ArrayList<>();
      for (Simcard simcard : simcards) {
         simcard.setPack(packNumber);
         updatedSimcards.add(simcardGateway.packSimcard(simcard));
      }
      return updatedSimcards;
   }

   public void assignPackToEmployee(int packNumber, long employeeIdentification) {
      simcardGateway.assignPackToEmployee(packNumber, employeeIdentification);
   }

   public void unassignPack(int packNumber) {
      simcardGateway.unassignPack(packNumber);
   }

   @Transactional
   public void removePackNumbers(List<Simcard> simcards) {
      simcards.forEach((simcard) -> {
         simcard.setPack(0);
         simcardGateway.updateSimcard(simcard);
      });
   }
}
