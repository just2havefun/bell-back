package org.belltronic.wholesalers.domain.usecase.subdistributor;

import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.entity.SubDistributor;
import org.belltronic.wholesalers.domain.gateway.SubdistributorGateway;
import org.belltronic.wholesalers.domain.usecase.employee.ListEmployeesUseCase;
import org.belltronic.wholesalers.domain.usecase.user.SendMailUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrudSubDistributorUseCase {

   @Autowired
   private SubdistributorGateway subdistributorGateway;
   @Autowired
   private ListEmployeesUseCase listEmployeesUseCase;
   @Autowired
   private SendMailUseCase sendUserMailUseCase;

   public SubDistributor readSubDistributor(final String name) {
      SubDistributor subDistributor = subdistributorGateway.readSubdistributor(name);
      setEmployees(subDistributor);
      return subDistributor;
   }

   public SubDistributor readSubdistributorByEmail(final String email) {
      SubDistributor subDistributor = subdistributorGateway.readSubdistributorByEmail(email);
      setEmployees(subDistributor);
      return subDistributor;
   }

   public SubDistributor updateSubDistributor(SubDistributor subDistributor) {
      SubDistributor storedSubDistributor = readSubDistributor(subDistributor.getName());
      SubDistributor updatedSubDistributor = subdistributorGateway.updateSubdistributor(subDistributor);

      if (!subDistributor.getEmail().equals("") && !storedSubDistributor.getEmail().equals(subDistributor.getEmail())) {
         MailMessage mailMessage = MailMessage.builder()
               .to(subDistributor.getEmail())
               .subject("Creación de usuario Belltronic ERP")
               .content("<h1>Asignaci&oacute;n Usuario Belltronic</h1><h2>Email: " + subDistributor.getEmail()
                     + "</h2><h2>Password: " + subDistributor.getPassword() + "</h2>")
               .contentType("text/html")
               .build();

         sendUserMailUseCase.sendSubDsitributorMail(subDistributor, mailMessage);
      }
      setEmployees(updatedSubDistributor);
      return updatedSubDistributor;
   }

   public List<SubDistributor> listSubDistributors() {
      List<SubDistributor> subDistributors = subdistributorGateway.listSubDistributors();
      setEmployees(subDistributors);
      return subDistributors;
   }

   public List<SubDistributor> listSubDistributorsByLeader(String leaderName) {
      List<SubDistributor> subDistributors = subdistributorGateway.listSubDistributorsByLeader(leaderName);
      setEmployees(subDistributors);
      return subDistributors;
   }

   private void setEmployees(List<SubDistributor> subDistributors) {
      subDistributors.parallelStream().forEach((this::setEmployees));
   }

   private void setEmployees(SubDistributor subDistributor) {
      subDistributor
            .setAdvisers(listEmployeesUseCase.listAdvisersByLeader(subDistributor.getName()));
      subDistributor.setWholesalers(
            listEmployeesUseCase.listWholesalersByLeader(subDistributor.getName()));
   }
}
