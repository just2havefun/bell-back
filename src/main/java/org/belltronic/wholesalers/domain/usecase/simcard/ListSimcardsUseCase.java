package org.belltronic.wholesalers.domain.usecase.simcard;

import org.belltronic.wholesalers.domain.entity.Simcard;
import org.belltronic.wholesalers.domain.gateway.SimcardGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListSimcardsUseCase {

   @Autowired
   private SimcardGateway simcardGateway;

   public List<Simcard> listFreesByOwner(String ownerName) {
      return simcardGateway.listSimcardsByTypeAndOwner(2, ownerName);
   }
}
