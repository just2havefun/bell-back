package org.belltronic.wholesalers.domain.usecase.report.recharge;

import org.belltronic.wholesalers.domain.entity.report.RechargeReport;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetRechargeReportUseCase {

   @Autowired
   private RechargeGateway rechargeGateway;

   public List<RechargeReport> getSubRechargeReport(final String subdistributor, final Integer year,
                                                    final Integer month) throws ParseException {
      return rechargeGateway.getSubRechargeReport(subdistributor, year, month)
            .stream()
            .filter(rechargeReport -> rechargeReport.getPrepaidRecharge().compareTo(BigDecimal.ZERO) != 0 ||
                  rechargeReport.getFreeRecharge().compareTo(BigDecimal.ZERO) != 0)
            .collect(Collectors.toList());
   }

}
