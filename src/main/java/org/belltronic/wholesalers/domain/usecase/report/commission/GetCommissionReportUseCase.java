package org.belltronic.wholesalers.domain.usecase.report.commission;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.entity.report.CommissionReport;
import org.belltronic.wholesalers.domain.entity.report.csv.DetailCommission;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.util.List;

@Service
public class GetCommissionReportUseCase {

   private static String[] headerRecord = {"NOMBRE", "NUMERO", "ICC", "TIPO", "FECHA ENTREGA",
         "FECHA ACTIVACION", "FECHA VENCIMIENTO", "ESTADO", "RECARGA", "NETO", "COMISION"};

   @Autowired
   private RechargeGateway rechargeGateway;

   public CommissionReport getCommissionReport(final Long employeeIdentification, final Integer year,
                                               final Integer month) throws ParseException {
      return rechargeGateway.getCommissionReport(employeeIdentification, year, month);
   }

   public List<CommissionReport> getCommissionsReport(final Long employeeIdentification) {
      return rechargeGateway.getCommissionsReport(employeeIdentification);
   }

   public List<CommissionReport> getCommissionReport(final String subDistributorName, final Integer year,
                                                     final Integer month) throws ParseException {
      return rechargeGateway.getSubCommissionsReport(subDistributorName, year, month);
   }

   public File downloadCommissionReport(Long identification, final Integer year,
                                        final Integer month) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, ParseException {
      List<DetailCommission> commissionDetails = rechargeGateway.getCommissionDetail(identification, year, month);
      File file = new File("simcards.csv");
      Writer writer = new FileWriter("simcards.csv");
      StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
      CSVWriter csvWriter = new CSVWriter(writer,
            CSVWriter.DEFAULT_SEPARATOR,
            CSVWriter.NO_QUOTE_CHARACTER,
            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
            CSVWriter.DEFAULT_LINE_END);
      csvWriter.writeNext(headerRecord);
      beanToCsv.write(commissionDetails);
      writer.close();
      return file;
   }
}
