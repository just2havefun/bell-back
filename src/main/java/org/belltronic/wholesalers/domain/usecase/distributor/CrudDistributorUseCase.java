package org.belltronic.wholesalers.domain.usecase.distributor;

import org.belltronic.wholesalers.domain.entity.Distributor;
import org.belltronic.wholesalers.domain.gateway.DistributorGateway;
import org.belltronic.wholesalers.domain.usecase.subdistributor.CrudSubDistributorUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrudDistributorUseCase {

   @Autowired
   private DistributorGateway distributorGateway;
   @Autowired
   private CrudSubDistributorUseCase crudSubDistributorUseCase;

   public Distributor readDistributorByEmail(String email) {
      Distributor distributor = distributorGateway.readDistributorByEmail(email);
      distributor.setSubDistributors(crudSubDistributorUseCase.listSubDistributorsByLeader(email));
      return distributor;
   }

   public List<Distributor> listDistributors() {
      return distributorGateway.listDistributors();
   }
}
