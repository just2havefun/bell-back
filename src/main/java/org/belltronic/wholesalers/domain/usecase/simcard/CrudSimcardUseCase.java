package org.belltronic.wholesalers.domain.usecase.simcard;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.entity.Simcard;
import org.belltronic.wholesalers.domain.errors.NotFoundException;
import org.belltronic.wholesalers.domain.gateway.EmployeeGateway;
import org.belltronic.wholesalers.domain.gateway.SimcardGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrudSimcardUseCase {

   @Autowired
   private SimcardGateway simcardGateway;
   @Autowired
   private EmployeeGateway employeeGateway;

   public Simcard readSimcard(String id) {
      Simcard simcard = simcardGateway.readSimcard(id);
      if (simcard.getEmployeeIdentification() != null) {
         try {
            Employee employee = employeeGateway.readEmployee(simcard.getEmployeeIdentification());
            simcard.setEmployeeName(employee.getName());
            simcard.setSubDistributorName(employee.getLeaderName());
         } catch (NotFoundException ne) {
            simcard.setEmployeeName(String.valueOf(simcard.getEmployeeIdentification()));
         }
      }
      return simcard;
   }

   public Simcard updateSimcard(String id, Simcard simcard) {
      Simcard storedSimcard = readSimcard(id);
      if (simcard.getEmployeeIdentification() != null) {
         Employee employee = employeeGateway.readEmployee(simcard.getEmployeeIdentification());
         storedSimcard.setEmployeeIdentification(simcard.getEmployeeIdentification());
         storedSimcard.setEmployeeName(employee.getName());
      } else {
         storedSimcard.setEmployeeIdentification(null);
      }
      storedSimcard.setDeliveryDate(simcard.getDeliveryDate());
      return simcardGateway.updateSimcard(storedSimcard);
   }
}
