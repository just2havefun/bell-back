package org.belltronic.wholesalers.domain.usecase.location;

import org.belltronic.wholesalers.domain.entity.Location;
import org.belltronic.wholesalers.domain.gateway.AdvisorGateway;
import org.belltronic.wholesalers.domain.gateway.LocationGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrudLocationUseCase {

   @Autowired
   private LocationGateway locationGateway;

   @Autowired
   private AdvisorGateway advisorGateway;

   public Location createLocation(Location location) {
      advisorGateway.readAdvisor(location.getEmployeeIdentification());
      return locationGateway.createLocation(location);
   }
}
