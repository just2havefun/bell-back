package org.belltronic.wholesalers.domain.usecase.report.commission;

import org.belltronic.wholesalers.domain.entity.CommissionHistory;
import org.belltronic.wholesalers.domain.gateway.ReportGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrudCommissionHistoryUseCase {

   @Autowired
   private ReportGateway reportGateway;

   public CommissionHistory createCommissionHistory(CommissionHistory commissionHistory) {
      return reportGateway.createCommissionHistory(commissionHistory);
   }

   public CommissionHistory getCommissionHistoryByPeriod(final Long employeeIdentification, final Integer year,
                                                         final Integer month) {
      return reportGateway.getCommissionHistoryByPeriod(employeeIdentification, year, month);
   }

   public void deleteCommissionHistory(final int id) {
      reportGateway.deleteCommissionHistory(id);
   }

   public List<CommissionHistory> listCommissionHistoryByEmployee(final Long employeeIdentification) {
      return reportGateway.listCommissionHistoryByEmployee(employeeIdentification);
   }
}
