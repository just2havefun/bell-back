package org.belltronic.wholesalers.domain.usecase.report.simcard;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.entity.report.csv.FreeReport;
import org.belltronic.wholesalers.domain.entity.report.csv.PrepaidReport;
import org.belltronic.wholesalers.domain.entity.report.csv.SimcardReport;
import org.belltronic.wholesalers.domain.gateway.ReportGateway;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetAssignationReportUseCase {

   private static final ModelMapper modelMapper = new ModelMapper();

   private static final String[] FREE_HEADERS = {"DISTRIBUIDOR", "SUBDISTRIBUIDOR", "MAYORISTA", "PAQUETE", "ORDEN", "NUMERO", "ICC", "FECHA ENTREGA", "FECHA ACTIVACION", "FECHA VENCIMIENTO", "ESTADO", "EMPRESA", "NIT", "DIRECCION", "COD_SCL", "COD_PUNTO",
         "CODIGO PLAN", "VALOR PLAN", "RESPONSABLE", "CEDULA", "DIRECCION RESPONSABLE", "CIUDAD RESPONSABLE", "BARRIO RESPONSABLE",
         "TELEFONO", "DETALLE LLAMADA", "FECHA LLAMADA"};

   private static final String[] PREPAID_HEADERS = {"DISTRIBUIDOR", "SUBDISTRIBUIDOR", "MAYORISTA", "PAQUETE", "ORDEN", "NUMERO", "ICC", "FECHA ENTREGA", "FECHA ACTIVACION", "FECHA VENCIMIENTO", "ESTADO"};

   private ReportGateway reportGateway;

   @Autowired
   public GetAssignationReportUseCase(ReportGateway reportGateway) {
      this.reportGateway = reportGateway;
   }

   public File getReport(int type, String subName, Date initialDate, Date finalDate) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
      List<SimcardReport> simcards;
      if (subName.equals("TODOS")) {
         simcards = reportGateway.listSimcardsByTypeAndDate(type, initialDate, finalDate);
      } else {
         simcards = reportGateway.listSimcardsByTypeAndOwnerAndDate(type, subName, initialDate, finalDate);
      }
      List<?> simcardsData;
      String[] headers;
      if (type == 1) {
         simcardsData = simcards.stream()
               .map(simcard -> modelMapper.map(simcard, PrepaidReport.class))
               .collect(Collectors.toList());
         headers = PREPAID_HEADERS;
      } else {
         simcardsData = simcards.stream()
               .map(simcard -> modelMapper.map(simcard, FreeReport.class))
               .collect(Collectors.toList());
         headers = FREE_HEADERS;
      }

      File file = new File("simcards-" + subName + ".csv");
      Writer writer = new FileWriter("simcards-" + subName + ".csv");
      StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
      CSVWriter csvWriter = new CSVWriter(writer,
            CSVWriter.DEFAULT_SEPARATOR,
            CSVWriter.NO_QUOTE_CHARACTER,
            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
            CSVWriter.DEFAULT_LINE_END);
      csvWriter.writeNext(headers);
      beanToCsv.write(simcardsData);
      writer.close();
      return file;
   }
}
