package org.belltronic.wholesalers.domain.usecase.employee;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.gateway.EmployeeGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrudEmployeeUseCase {

   @Autowired
   private EmployeeGateway employeeGateway;

   public Employee createEmployee(Employee employee) {
      return employeeGateway.createEmployee(employee);
   }

   public Employee readEmployee(final Long identification) {
      return employeeGateway.readEmployee(identification);
   }

   public Employee updateEmployee(final Long identification, final Employee employee) {
      return employeeGateway.updateEmployee(identification, employee);
   }

   public void deleteEmployee(final Long identification) {
      employeeGateway.deleteEmployee(identification);
   }
}
