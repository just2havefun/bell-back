package org.belltronic.wholesalers.domain.usecase.file.updateNumbersUseCase;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.errors.DatabaseException;
import org.belltronic.wholesalers.domain.gateway.MailGateway;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.belltronic.wholesalers.domain.gateway.SimcardGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UpdateNumbersFileUseCase {

   private static final Logger LOGGER = LoggerFactory.getLogger(UpdateNumbersFileUseCase.class);
   private static String[] headerRecord = {"ICC", "NUMERO", "RESULTADO"};

   @Autowired
   private SimcardGateway simcardGateway;
   @Autowired
   private RechargeGateway rechargeGateway;
   @Autowired
   private MailGateway sendEmail;

   @Async
   public void updateNumbersByIcc(final File file, final String receiver) {
      try {
         CSVReader csvReader = new CSVReaderBuilder(new FileReader(file)).build();

         String[] record;
         List<UpdateNumberStatus> status = new ArrayList<>();
         LOGGER.info("Cleaning recharges");
         rechargeGateway.cleanRecharges();
         LOGGER.info("Recharges cleaned");
         while ((record = csvReader.readNext()) != null) {
            String icc = record[0];
            String number = record[1];
            try {
               LOGGER.info("Updating simcard with Icc: " + icc);
               simcardGateway.deleteConflictedNumberByIcc(number, icc);
               simcardGateway.updateNumberByIcc(number, icc);
               status.add(UpdateNumberStatus.builder()
                     .icc(icc)
                     .number(number)
                     .status("Exitoso")
                     .build());
            } catch (DatabaseException e) {
               status.add(UpdateNumberStatus.builder()
                     .icc(icc)
                     .number(number)
                     .status("Error: " + e.getMessage())
                     .build());
            }
         }
         sendReportMail(status, receiver);
      } catch (IOException e) {
         LOGGER.error("Error Processing file: " + file.getName(), e);
         sendErrorMail(receiver);
      }
   }

   private void sendErrorMail(final String receiver) {

      MailMessage mailMessage = MailMessage.builder()
            .to(receiver)
            .subject("Reporte Actualización numeros por icc")
            .content("<h1>El archivo que esta intentado subir esta corrupto</h1>")
            .contentType("text/html")
            .build();
      sendEmail.sendEmail(mailMessage);
   }

   private void sendReportMail(List<UpdateNumberStatus> statuses, String receiver) {

      try {
         File file = File.createTempFile("uploadNumbers", ".csv");
         Writer writer = new FileWriter(file);

         StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
         CSVWriter csvWriter = new CSVWriter(writer,
               CSVWriter.DEFAULT_SEPARATOR,
               CSVWriter.NO_QUOTE_CHARACTER,
               CSVWriter.DEFAULT_ESCAPE_CHARACTER,
               CSVWriter.DEFAULT_LINE_END);
         csvWriter.writeNext(headerRecord);
         beanToCsv.write(statuses);
         writer.close();

         MailMessage mailMessage = MailMessage.builder()
               .to(receiver)
               .subject("Reporte Actualización numeros por icc")
               .content("<h1>Adjunto encontrara el reporte de actualización</h1>")
               .contentType("text/html")
               .attachments(Collections.singletonList(file))
               .build();
         sendEmail.sendEmail(mailMessage);
      } catch (IOException | CsvRequiredFieldEmptyException | CsvDataTypeMismatchException e) {
         LOGGER.error("Error Sending Mail of upload numbers: ", e);
      }
   }


}
