package org.belltronic.wholesalers.domain.usecase.user.updateuser;

import org.belltronic.wholesalers.domain.entity.ApplicationUser;
import org.belltronic.wholesalers.domain.entity.MailMessage;
import org.belltronic.wholesalers.domain.gateway.UserGateway;
import org.belltronic.wholesalers.domain.usecase.user.SendMailUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateUserUseCase {

   @Autowired
   private UserGateway userGateway;
   @Autowired
   private SendMailUseCase sendUserMailUseCase;

   public ApplicationUser updateUser(final String email, final ApplicationUser applicationUserDTO) {
      ApplicationUser user = userGateway.updateUser(email, applicationUserDTO);

      MailMessage mailMessage = MailMessage.builder()
            .to(user.getEmail())
            .subject("Actualización de credenciales Belltronic ERP")
            .content("<h1>Se han actualizado sus credenciales</h1><h2>Email: " + user.getEmail() + "</h2><h2>Password: "
                  + applicationUserDTO.getPassword() + "</h2>")
            .contentType("text/html")
            .build();

      sendUserMailUseCase.sendUserMail(applicationUserDTO, mailMessage);

      return user;
   }
}
