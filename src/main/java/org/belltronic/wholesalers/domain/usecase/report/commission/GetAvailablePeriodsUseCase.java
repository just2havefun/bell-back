package org.belltronic.wholesalers.domain.usecase.report.commission;

import org.belltronic.wholesalers.domain.entity.Employee;
import org.belltronic.wholesalers.domain.gateway.RechargeGateway;
import org.belltronic.wholesalers.domain.usecase.employee.CrudEmployeeUseCase;
import org.belltronic.wholesalers.domain.usecase.employee.ListEmployeesUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class GetAvailablePeriodsUseCase {

   @Autowired
   private RechargeGateway rechargeGateway;
   @Autowired
   private CrudEmployeeUseCase crudEmployeeUseCase;
   @Autowired
   private ListEmployeesUseCase listEmployeesUseCase;

   public Set<String> getAvailablePeriodsUseCase(final Long employeeIdentification) {
      Employee employeeDTO = crudEmployeeUseCase.readEmployee(employeeIdentification);
      return rechargeGateway.getAvailablePeriods(employeeDTO.getIdentification());
   }

   public Set<String> getSubAvailablePeriodsUseCase(final String name) {
      List<Employee> employees = listEmployeesUseCase.listWholesalersByLeader(name);
      Set<String> periods = new HashSet<>();
      for (Employee employeeDTO : employees) {
         periods.addAll(rechargeGateway.getAvailablePeriods(employeeDTO.getIdentification()));
      }
      return periods;
   }
}
